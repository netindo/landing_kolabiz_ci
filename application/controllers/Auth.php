<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 19/01/2021
 * Time: 16:18
 */

class Auth extends CI_Controller
{
    public function index(){
        $data['setting']     = $this->m_crud->read_data("setting","*")->row_array();

        if($this->session->id==''||$this->session->id==null){
            $this->load->view("bo/auth",$data);
        }
        else{
            $get_user = $this->m_crud->read_data("user", "*", "sess='".$this->session->id."'")->row_array();
            if ($get_user == null) {
                $this->session->sess_destroy();
                $this->load->view("bo/auth",$data);
            }else if ($this->session->id!=$get_user['sess']) {
                $this->session->sess_destroy();
                $this->load->view("bo/auth",$data);
            }else{
                redirect('bo');
            }
        }
    }
    public function login(){
        $result = array();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $get_user = $this->m_crud->read_data("user", "*", "username='".$username."'")->row_array();
        if ($get_user != null) {
            if (password_verify($password, $get_user['password'])) {
                $result['status'] = true;
                $options = array('cost' => 12);
                $result['res_login'] = array(
                    'id'=>password_hash($get_user['id'], PASSWORD_BCRYPT, $options),
                    'username'=>strtoupper($get_user['username']),
                    'level'=>strtoupper($get_user['level']),
                    'isLogin' => true,
                );
                $result['res_login']['sess']=$result['res_login']['id'];
                $this->m_crud->update_data('user', array('created_at'=>date('Y-m-d h:i:s'),'sess'=>$result['res_login']['id']),"username='".$_POST['username']."'");
                $this->session->set_userdata($result['res_login']);
            } else {
                $result['status'] = false;
                $result['message'] ='Invalid email or password!';
            }
        } else {
            $result['status'] = false;
            $result['message'] = 'User Not Found.';
        }

        echo json_encode($result);
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect();
    }
}