<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 14:47
 */

class Bo extends CI_Controller
{

    public function checkingRoute(){
        if($this->session->id==''||$this->session->id==null){
            $this->session->sess_destroy();
            redirect('auth');
        }
        else{
            $get_user = $this->m_crud->read_data("user", "*", "sess='".$this->session->id."'")->row_array();
            if ($get_user == null) {
                $this->session->sess_destroy();
                redirect('auth');
            }
            if ($this->session->id!=$get_user['sess']) {
                $this->session->sess_destroy();
                redirect('auth');
            }
        }

    }
    public function index(){
        $this->checkingRoute();
        $data=array('page'=>'dashboard','content'=>'dashboard');
        $this->load->view("bo/index",$data);
    }


    public function dashboard($action=null){
        $table 						= 'visitor';
        if($action == 'year'){
            $grafik 	= $this->m_crud->read_data($table,
                "YEAR(date_$table) AS 'year', COUNT(ip_$table) AS total",
                null,null, "YEAR(date_$table)"
            )->result_array();
            foreach($grafik as $row){
                $year[] 	= $row['year'];
                $total[] = (float)$row['total'];
            }
            echo json_encode(array("year"=>$year,"total"=>$total));
        }
        elseif($action == "month") {
            $where 		= null;
            $year 	= $this->input->post("tahun");
            $year_now = "YEAR(date_$table)=YEAR(NOW())";
            $year_req = "YEAR(date_$table)='$year'";
            $where 		.= $year == null ? $year_now : $year_req;
            $grafik 	= $this->m_crud->read_data($table,
                "DATE_FORMAT(date_$table,'%m') AS valbulan,date_$table,YEAR(date_$table) AS 'thn', MONTH(date_$table) AS 'bln', COUNT(ip_$table) AS jml,
					CONCAT(
						CASE MONTH(date_$table)
						WHEN 1 THEN 'Jan' WHEN 2 THEN 'Feb' WHEN 3 THEN 'Mar' WHEN 4 THEN 'Apr' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Jun'
					 	WHEN 7 THEN 'Jul' WHEN 8 THEN 'Aug' WHEN 9 THEN 'Sep' WHEN 10 THEN 'Oct' WHEN 11 THEN 'Nov' WHEN 12
					 	THEN 'Dec' END
					) AS bulan
				", $where,null, "MONTH(date_$table)"
            )->result_array();
            foreach($grafik as $row){
                $month[] = $row['bulan'];
                $value[] = (float)$row['jml'];
            }
            $data = array(
                "bln" => $month,
                "jml" => $value,
                'grafik' => $grafik
            );
            echo json_encode($data);
        }
        elseif($action=='hari') {
            $where = null;
            $tahun = $this->input->post("tahun");
            $bulan = $this->input->post("bulan");
            if($tahun && $bulan){
                $where.= "YEAR(date_$table)='$tahun' AND MONTH(date_$table)='$bulan'";
            }else{
                $where.="YEAR(date_$table)=YEAR(NOW()) AND MONTH(date_$table)=MONTH(NOW())";
            }
            $grafik = $this->m_crud->read_data($table,
                "
					YEAR(date_$table) AS tahun, COUNT(ip_$table) AS jumlah_ip,date_$table,
  				CONCAT(
						CASE DAYOFWEEK(date_$table)
						WHEN 1 THEN 'Minggu' WHEN 2 THEN 'Senin' WHEN 3 THEN 'Selasa' WHEN 4 THEN 'Rabu' WHEN 5 THEN 'Kamis' WHEN 6 THEN 'Jumat' WHEN 7 THEN 'Sabtu' END
					) AS hari,
					CONCAT(
						CASE MONTH(date_$table)
						WHEN 1 THEN 'Januari' WHEN 2 THEN 'Februari' WHEN 3 THEN 'Maret' WHEN 4 THEN 'April' WHEN 5 THEN 'Mei' WHEN 6 THEN 'Juni' WHEN 7 THEN 'Juli' WHEN 8
					 	THEN 'Agustus'
						WHEN 9 THEN 'September' WHEN 10 THEN 'Oktober' WHEN 11 THEN 'November' WHEN 12 THEN 'Desember' END
					) AS bulan
				",
                $where,null, "DAY(date_$table)"
            )->result_array();
            foreach ($grafik as $row) {
                $day[] = $row['hari'];
                $tgl[] = date("D d-m-y",strtotime($row["date_$table"]));
                $val[] = (float)$row['jumlah_ip'];
            }
            $data = array(
                "hari" 		=> $day,
                "jumlah" 	=> $val,
                "tgl" 		=> $tgl,
                "grafik" 	=> $grafik
            );
            echo json_encode($data);
        }
        elseif ($action == "get"){
            $grafik = $this->m_crud->read_data($table,
                "browser_$table AS browser,COUNT(browser_$table) AS total_device",
                null,null,"browser_$table"
            )->result_array();
            foreach($grafik as $row){
                $jumlah[] 	= (float)$row['total_device'];
                $perangkat[]= $row['browser'];
            }
            $data 	= array(
                "perangkat"=>$perangkat,"jumlah"=>$jumlah
            );
            echo json_encode($data);
        }

    }
    public function slider($action=null,$page=1){
        $this->checkingRoute();
        $function 		    = 'slider';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=1';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                foreach ($result as $row){
                    $response.=/** @lang text */'
                        <div class="col-12 height-card box-margin" style="width:100%!important">
                        <div class="card">
                            <div class="card-body" style="width:100%!important">
                                <ul class="list-unstyled">
                                    <li class="media">
                                        <img src="'.base_url().$row["image"].'" class="mr-3 media-thumb" alt="...">
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-6 col-xs-6 col-md-6">
                                                    <h5 class="mt-0 mb-1 font-17 text-left">'.$row["title"].'</h5>
                                                </div>
                                                <div class="col-6 col-xs-6 col-md-6">
                                                    <div class="dashboard-dropdown text-right">
                                                        <div class="dropdown">
                                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dashboardDropdown51" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="text-white ti-more"></i></button>
                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown51">
                                                                <a class="dropdown-item"  onclick="edit(\'' . $row['id'] . '\')"><i class="ti-pencil-alt"></i> Edit</a>
                                                                <a class="dropdown-item"  onclick="hapus(\'' . $row['id'] . '\')"><i class="ti-trash"></i> Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           '.$row["description"].'
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    ';
                }
            }
            else{
                $response.=/** @lang text */'
                   <div class="col-md-12">
                        <div class="text-center">
                            <img src="'.$this->config->item("no_image").'" class="rounded" alt="...">
                        </div>
                    </div>
                ';
            }
            echo json_encode(array(
                'result'=>$response,
                'pagination_link' => $pagin['pagination_link']
            ));
        }
        else{
            $this->load->view("bo/index",$data);

        }
    }
    public function about($action=null){
        $this->checkingRoute();
        $data=array('page'=>'about','content'=>'about');
        if($action=='load_data'){
            $response='';
            $result=$this->m_crud->read_data("section","*","type=2")->row_array();
            $response.='
                <div class="card">
                <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6 col-12">
                        <img src="'.base_url().$result["image"].'" class="img-fluid rounded" alt="">
                    </div><!--end col-->
                    <div class="col-lg-7 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div class="section-title ml-lg-4">
                            <h4 class="title mb-4">'.$result["title"].'</h4>
                            <p class="text-muted">'.$result["description"].'</p>
                            <a href="javascript:void(0)" class="btn btn-primary mt-3">Join now</a>
                            <button  onclick="edit(\'' . $result['id'] . '\')" class="btn btn-primary mt-3" style="float: right">Ubah</button>
                        </div>
                    </div>
                </div>
</div>
</div>
            ';
            echo json_encode(array("result"=>$response));
        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function service($action=null,$page=1){
        $this->checkingRoute();


        $function 		    = 'service';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=3';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                $no =$pagin['start']+1;
                foreach ($result as $row){
                    $response.='
                        <tr>
                            <td>'.$no++.'</td>
                            <td>
                                <button onclick="edit(\'' . $row['id'] . '\')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button onclick="hapus(\'' . $row['id'] . '\')" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                            </td>
                            <td>'.$row["title"].'</td>
                            <td>'.$row["description"].'</td>
                            <td><i class="uil uil-'.$row["image"].'"></i></td>
                            <td>'.date("Y-m-d",strtotime($row["created_at"])).'</td>
                        </tr>
                    ';
                }
            }
            else{
                $response.='
                    <tr><td colspan="6"><center><img src="'.$this->config->item("no_image").'" alt=""></center></td></tr>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function rules($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'rules';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=4';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                foreach ($result as $row){
                    $response.='
                    <div class="col-12 col-xs-12 col-sm-6 col-md-3" style="margin-bottom: 10px">
                        <div class="card work-process border-0 text-center rounded shadow pricing-rates">
                            <div class="card-body">
                                <img src="'.base_url().$row["image"].'" class="avatar avatar-small mb-3" alt="">
                                <h4 class="title">'.$row["title"].'</h4>
                                <p class="text-muted para">
                                    '.$row["description"].'
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="padding-right:0">
                                    <button onclick="edit(\'' . $row['id'] . '\')" class="btn-block btn btn-primary"><i class="fa fa-pencil"></i></button>
                                </div>
                                <div class="col-md-6" style="padding-left: 0">
                                    <button onclick="hapus(\'' . $row['id'] . '\')" class="btn-block btn btn-danger"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    ';
                }
            }
            else{
                $response.=/** @lang text */'
                   <div class="col-md-12">
                        <div class="text-center">
                            <img src="'.$this->config->item("no_image").'" class="rounded" alt="...">
                        </div>
                    </div>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function bimtek($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'bimtek';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= 'konsultan_budidaya';
        $where = 'type=5';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                $no =$pagin['start']+1;
                foreach ($result as $row){
                    $response.='
                        <tr>
                            <td>'.$no++.'</td>
                            <td style="width: 10%">
                                <button onclick="edit(\'' . $row['id'] . '\')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button onclick="hapus(\'' . $row['id'] . '\')" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                            </td>
                            <td>'.$row["title"].'</td>
                            <td>'.$row["description"].'</td>
                            <td style="width: 10%">'.date("Y-m-d",strtotime($row["created_at"])).'</td>
                        </tr>
                    ';
                }
            }
            else{
                $response.='
                    <tr><td colspan="5"><center><img src="'.$this->config->item("no_image").'" alt=""></center></td></tr>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function testimoni($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'testimoni';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=6';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                foreach ($result as $row){
                    $response.='
                    <div class="vertical-timeline-block">
                        <div class="vertical-timeline-icon">
                            <img style="border-radius: 50%!important;" src="'.base_url().$row["image"].'" alt="">
                        </div>

                        <div class="vertical-timeline-content">
                            <h5>'.$row["title"].'</h5>
                            <p>'.$row["description"].'</p>
                            <span class="vertical-date">'.date("Y-m-d",strtotime($row["created_at"])).'</span>
                            <span style="float: right" class="vertical-date">
                                <button onclick="edit(\'' . $row['id'] . '\')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button onclick="hapus(\'' . $row['id'] . '\')" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                            
                            </span>
                        </div>
                    </div>
                    ';
                }
            }
            else{
                $response.=/** @lang text */'
                   <div class="col-md-12">
                        <div class="text-center">
                            <img src="'.$this->config->item("no_image").'" class="rounded" alt="...">
                        </div>
                    </div>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function kerjasama($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'kerjasama';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=7';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at asc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                $no =$pagin['start']+1;
                foreach ($result as $row){
                    $response.='
                        <tr>
                            <td>'.$no++.'</td>
                            <td style="width: 10%">
                                <button onclick="edit(\'' . $row['id'] . '\')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button onclick="hapus(\'' . $row['id'] . '\')" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                            </td>
                            <td>'.$row["description"].'</td>
                        </tr>
                    ';
                }
            }
            else{
                $response.='
                    <tr><td colspan="6"><center><img src="'.$this->config->item("no_image").'" alt=""></center></td></tr>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function product($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'product';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=8';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at desc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                foreach ($result as $row){
                    $status='';
                    $badge='';
                    if((int)$row['link']<100){
                        $status.='Belum Selesai';
                        $badge='ribbon-danger';
                    }else{
                        $status.='Selesai';
                        $badge='ribbon-success';

                    }
                    $response.='
                    <div class="col-md-3 col-xl-3 height-card box-margin">
                        <div class="card">
                            <div class="ribbon ribbon-bookmark ribbon-right '.$badge.'">'.$status.'</div>
            
                            <img class="img-fluid" style="height:150px" src="'.base_url().$row["image"].'" alt="">
                            <div class="card-body">
                                <p class="mb-0 mb-2">'.$row["title"].'</p>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="progress h-8 mb-0 h-8">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: '.(int)$row["link"].'%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <br>
                                        <ul class="list-unstyled mb-0 numbers">
                                            <li><i class="fa fa-clock-o text-muted"></i> '.(int)$row["link"].'%</li>
                                            <li><i class="fa fa-map-marker text-muted"></i> '.$row["description"].'</li>
                                        </ul>
            
                                    </div>
                                </div>
                                <div class=" d-flex align-items-center justify-content-between">
                                    <div class="widgets-card-title">
                                    </div>
                                    <div class="dashboard-dropdown">
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="dashboardDropdown50" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-more"></i></button>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dashboardDropdown50" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 30px, 0px);">
                                                <a class="dropdown-item" href="#"  onclick="edit(\'' . $row['id'] . '\')"><i class="ti-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item" href="#"  onclick="hapus(\'' . $row['id'] . '\')"><i class="ti-trash"></i> Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                            </div>
                        </div>
                    </div>
                    ';
                }
            }
            else{
                $response.=/** @lang text */'
                <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                     <div class="text-center">
                            <img src="'.$this->config->item("no_image").'" class="rounded" alt="...">
                        </div>
                    </div>
                   </div>  
                       
                    </div>
                   
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function gallery($action=null,$page=1){
        $this->checkingRoute();

        $function 		    = 'gallery';
        $table 			= 'gallery';
        $data['title'] 	= 'gallery';
        $data['content']= $function;
        $data['page'] 	= $function;
//        $data['result']=$this->m_crud->read_data();
        $where = '';
        $search = $this->input->post('any',true);
        if($action=='load_data'){
            $response = $this->m_crud->read_data("album","*","type=1","id DESC")->result_array();
            echo json_encode(array('result'=>$response));
        }
        elseif($action=='simpan'){
            $data=array();
            foreach ($_POST['image'] as $row){
                array_push($data,$row['image']);
                $this->m_crud->create_data("album",array("image"=>$row["image"],"title"=>"-","type"=>1));
            }
//            $this->m_crud->create_data("album",array("image"=>));
//            for($i=0;$i<$_POST['image'];$i++){
//                $data['image'] = $_POST['image'][$i];
//            }
            if((array)count($data)==(array)count($_POST['image'])){
                echo json_encode(array("status"=>true));

            }
            else{
                echo json_encode(array("status"=>false));
            }
        }
        else{
            $this->load->view("bo/index",$data);

        }

    }
    public function socmed($action=null,$page=1){
        $this->checkingRoute();
        $function 		    = 'socmed';
        $table 			= 'section';
        $data['title'] 	= 'section';
        $data['content']= $function;
        $data['page'] 	= $function;
        $where = 'type=10';
        $search = $this->input->post('any',true);
        $response='';
        if($search&&$search!=null){
            $where .= " AND ";
            $where .= "(title like '%".$search."%' or description like '%".$search."%')";
        }
        if($action=='load_data'){
            $response='';
            $pagin = $this->m_crud->myPagination('',4,$table,'id',null,null,$where,10,$page);
            $result=$this->m_crud->read_data($table,"*",$where,'created_at asc',null,$pagin["per_page"], $pagin['start'])->result_array();
            if($result!=null){
                $no =$pagin['start']+1;
                foreach ($result as $row){
                    $response.='
                        <tr>
                            <td>'.$no++.'</td>
                            <td style="width: 10%">
                                <button onclick="edit(\'' . $row['id'] . '\')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>
                                <button onclick="hapus(\'' . $row['id'] . '\')" class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                            </td>
                            <td><img src="'.base_url().$row["image"].'" class="rounded" style="max-height: 30px" alt=""></td>
                            <td>'.$row["title"].'</td>
                            <td><a href="'.$row["link"].'" target="_blank">'.$row["link"].'</a></td>
                        </tr>
                    ';
                }
            }
            else{
                $response.='
                    <tr><td colspan="5"><center><img src="'.$this->config->item("no_image").'" alt=""></center></td></tr>
                ';
            }
            echo json_encode(array(
                "result"=>$response,
                'pagination_link' => $pagin['pagination_link']

            ));

        }
        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function setting($action=null){
        $this->checkingRoute();
        $data=array('page'=>'setting','content'=>'setting');
        if($action=='load_data'){
            $get_user = $this->m_crud->read_data("user", "*", "sess='".$this->session->id."'")->row_array();
            $get_setting = $this->m_crud->read_data("setting","*")->row_array();
            echo json_encode(array(
                'user'=>$get_user,
                'setting'=>$get_setting
            ));
        }
        else if($action=='update_user'){
            $new_password = $this->input->post("password",true);
            $new_username = $this->input->post("username",true);
            $options = array('cost' => 12);
            $password = password_hash($new_password, PASSWORD_BCRYPT, $options);
            $data=array("username"=>$new_username);
            $get_user = $this->m_crud->read_data("user", "*", "sess='".$this->session->id."'")->row_array();
            if($new_password!=''||$new_password!=null){
                if($password!=$get_user['password']){
                    $data['password'] = $password;
                }
            }
            $this->m_crud->update_data("user",$data,"sess='".$this->session->id."'");
            $this->session->sess_destroy();
            echo json_encode(array("result"=>$data,"status"=>true));
        }
        else if($action=='update_company'){
            $newData=array(
                "name"=>$this->input->post('name',true),
                "email"=>$this->input->post('email',true),
                "telephone"=>$this->input->post('telephone',true),
                "address"=>$this->input->post('address',true),
            );
            $path = 'assets/images/logo';
            $config['upload_path']          = './'.$path;
            $config['allowed_types']        = 'bmp|gif|jpg|jpeg|png|svg';
            $config['max_size']             = 5120;
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $input_file = array('1'=>'file_upload');
            $valid = true;
            foreach($input_file as $row){
                if( (! $this->upload->do_upload($row)) && $_FILES[$row]['name']!=null){
                    $file[$row]['file_name']=null;
                    $file[$row] = $this->upload->data();
                    $valid = false;
                    $data['error_'.$row] = $this->upload->display_errors();
                    break;
                } else{
                    $file[$row] = $this->upload->data();
                    $data[$row] = $file;
                    if($file[$row]['file_name']!=null){
                        $manipulasi['image_library'] 	= 'gd2';
                        $manipulasi['source_image'] 	= $file[$row]['full_path'];
                        $manipulasi['maintain_ratio'] 	= true;
                        $manipulasi['width']         	= 5120;
                        $this->load->library('image_lib', $manipulasi);
                        $this->image_lib->resize();
                    }
                }
            }
            if($_FILES['file_upload']['name']!=null) {
                $newData["logo"] = ($_FILES['file_upload']['name']!=null)?($path.'/'.$file['file_upload']['file_name']):null;
                if($_POST['file_uploaded']!=null||$_POST['file_uploaded']!=''){
                    unlink($_POST['file_uploaded']);
                }
            }

            $this->m_crud->update_data('setting', $newData,"id='".$this->input->post('id_company')."'");
            echo json_encode(array("result"=>$data,"status"=>true));

        }
        else if($action=='update_other'){
            $newData=array(
                "version"=>$this->input->post('version',true),
                "url"=>$this->input->post('url',true),
                "wa_message"=>$this->input->post('wa_message',true),
            );
            $this->m_crud->update_data('setting', $newData,"id='".$this->input->post('id_setting')."'");
            echo json_encode(array("result"=>$data,"status"=>true));
        }
        else if($action=='update_privacy_policy'){
            $newData=array(
                "privacy_policy"=>$this->input->post('desc_privacy',true),
            );
            $this->m_crud->update_data('setting', $newData,"id='".$this->input->post('id_desc_privacy')."'");
            echo json_encode(array("result"=>$this->input->post(),"status"=>true));
        }
        else if($action=='update_faq'){
            $newData=array(
                "faq"=>$this->input->post('desc_faq',true),
            );
            $this->m_crud->update_data('setting', $newData,"id='".$this->input->post('id_desc_faq')."'");
            echo json_encode(array("result"=>$data,"status"=>true));
        }

        else{
            $this->load->view("bo/index",$data);
        }
    }
    public function section($action=null){
        $this->checkingRoute();

        $response=array();
        if($action=='save'){
            $input=$this->input->post();
            $newData=array(
                "title"=>$this->input->post('title',true),
                "link"=>$this->input->post('link',true),
                "link_video"=>$this->input->post('link_video',true),
                "description"=>$this->input->post('description',true),
                "type"=>$this->input->post('type',true),
            );
            if($input['page']=='socmed'||$input['page']=='slider'||$input['page']=='about'||$input['page']=='rules'||$input['page']=='testimoni'||$input['page']=='product'){
                $path = 'assets/images/section';
                $config['upload_path']          = './'.$path;
                $config['allowed_types']        = 'bmp|gif|jpg|jpeg|png|svg';
                $config['max_size']             = 5120;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $input_file = array('1'=>'file_upload');
                $valid = true;
                foreach($input_file as $row){
                    if( (! $this->upload->do_upload($row)) && $_FILES[$row]['name']!=null){
                        $file[$row]['file_name']=null;
                        $file[$row] = $this->upload->data();
                        $valid = false;
                        $data['error_'.$row] = $this->upload->display_errors();
                        break;
                    } else{
                        $file[$row] = $this->upload->data();
                        $data[$row] = $file;
                        if($file[$row]['file_name']!=null){
                            $manipulasi['image_library'] 	= 'gd2';
                            $manipulasi['source_image'] 	= $file[$row]['full_path'];
                            $manipulasi['maintain_ratio'] 	= true;
                            $manipulasi['width']         	= 500;
                            $this->load->library('image_lib', $manipulasi);
                            $this->image_lib->resize();
                        }
                    }
                }
                if($_FILES['file_upload']['name']!=null) {
                    $newData["image"] = ($_FILES['file_upload']['name']!=null)?($path.'/'.$file['file_upload']['file_name']):null;
                    if($_POST['file_uploaded']!=null||$_POST['file_uploaded']!=''){
                        unlink($_POST['file_uploaded']);
                    }

                }
            }
            if($_POST['page']=='service'||$_POST['page']=='konsultan_budidaya'){
                $newData['image'] = $this->input->post('image',true);
            }



            if($input['param']=='add'){
                $this->m_crud->create_data('section', $newData);
                $response['status']=true;
//                echo json_encode(array("status"=>true));
            }
            else{
                $newData['id'] = $input['id'];
                $this->m_crud->update_data('section', $newData,"id='".$input['id']."'");
//                echo json_encode(array("status"=>$data));
            }
            echo json_encode(array("status"=>$_POST['page'],'data'=>$newData));

        }
        else if ($action == 'edit') {
            $get_data = $this->m_crud->read_data('section', "*", "id = '".$_POST['id']."'")->row_array();
            $result = array();
            if ($get_data != null) {
                $result['status'] = true;
                $result['result'] = $get_data;
            } else {
                $result['status'] = false;
            }

            echo json_encode($result);
        }
        else if ($action == 'hapus') {
            $id=$this->input->post("id",true);
            $get_id = $this->m_crud->read_data("section","image",array("id" => $id))->row_array();
            unlink($get_id["image"]);
            $delete_data = $this->m_crud->delete_data('section', "id = '".$id."'");
            if ($delete_data) {
                $status = $get_id;
            } else {
                $status = $get_id;
            }
            echo $status;
//            echo json_encode(array('status'=>$get_id));
        }

    }
    public function album($action=null){
        $this->checkingRoute();
        $response=array();
        if($action==='save'){
            for($i=0;$i<count($_FILES['image']['name']);$i++) {
                $path = 'assets/images/album';
                $_FILES['file']['name']		= $_FILES['image']['name'][$i];
                $_FILES['file']['type'] 	= $_FILES['image']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'][$i];
                $_FILES['file']['error'] 	= $_FILES['image']['error'][$i];
                $_FILES['file']['size'] 	= $_FILES['image']['size'][$i];
                $config['upload_path']    	= './'.$path;
                $config['allowed_types'] 	= 'jpg|jpeg|png|gif|svg';
                $config['file_name'] 		= $_FILES['image']['name'][$i];
                $config['image_library'] 	= 'gd2';
                $config['maintain_ratio'] 	= true;
                $config['width']         	= 500;
                $config['max_size']       	= 5120;
                $config['encrypt_name'] 	= TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('file')) {
                    $uploadData = $this->upload->data();
                    $filename 	= $path.'/'.$uploadData['file_name'];
                    $data = array(
                        "image"		=> $filename,
                        "title"		=> $_POST['title'][$i],
                        "type"		=> $_POST['type'],
                    );
                    $response 		= array("pesan" => '');
                    $this->m_crud->create_data('album',$data);
                }else{
                    $response = array("pesan" => $this->upload->display_errors());
                }
            }
            echo json_encode($response);
        }
        else if ($action == 'hapus') {
            $id=$this->input->post("id",true);
            $get_id = $this->m_crud->read_data("album","image",array("id" => $id))->row_array();
            unlink($get_id["image"]);
            $delete_data = $this->m_crud->delete_data('album', "id = '".$id."'");
//            if ($delete_data) {
//                $status = $get_id;
//            } else {
//                $status = $get_id;
//            }
//            echo $status;
            echo json_encode(array('status'=>true));
        }

    }

}