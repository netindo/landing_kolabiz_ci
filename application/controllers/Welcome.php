<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 11:22
 */

class Welcome extends CI_Controller
{
    public function createController($name){
        $data = '<?php class '.$name.' extends CI_Controller{
            public function index(){
                $this->load->view("'.$name.'");
            }
            public function insert(){
            }
            public function edit(){
            }
            public function update(){
                
            }
            public function delete(){
                
            }
        }';
        write_file('application/controllers/'.$name.'.php', $data);
        write_file('application/views/'.$name.'.php', $this->htmlTag(''));
    }

    public function htmlTag($content){
        $url1=base_url().'assets/';
        return '
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8" />
                <title></title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
                <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
                <meta name="author" content="Shreethemes" />
                <meta name="email" content="shreethemes@gmail.com" />
                <meta name="website" content="http://www.shreethemes.in/" />
                <meta name="Version" content="v2.6" />
                <link rel="preconnect" href="https://fonts.gstatic.com">
                <link href="https://fonts.googleapis.com/css2?family=Edu+SA+Beginner&family=Signika+Negative:wght@300&display=swap" rel="stylesheet">
                <link rel="shortcut icon">
                <link href="' .$url1.'css/bootstrap.min.css" rel="stylesheet" type="text/css" />
                <link href="' .$url1.'css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
                <link href="' .$url1.'css/line.css">
                <link href="' .$url1.'css/magnific-popup.css" rel="stylesheet" type="text/css" />
                <link href="' .$url1.'css/owl.carousel.min.css"/>
                <link href="' .$url1.'css/owl.theme.default.min.css"/>
                <link href="' .$url1.'css/flexslider.css" rel="stylesheet" type="text/css" />
                <link href="' .$url1.'css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
                <link href="' .$url1.'css/colors/default.css" rel="stylesheet" id="color-opt">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.css" integrity="sha512-4a1cMhe/aUH16AEYAveWIJFFyebDjy5LQXr/J/08dc0btKQynlrwcmLrij0Hje8EWF6ToHCEAllhvGYaZqm+OA==" crossorigin="anonymous" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" integrity="sha512-nIm/JGUwrzblLex/meoxJSPdAKQOe2bLhnrZ81g5Jbh519z8GFJIWu87WAhBH+RAyGbM4+U3S2h+kL5JoV6/wA==" crossorigin="anonymous" />
            </head>
            <body>
                <header id="topnav" class="defaultscroll sticky">
                     <div class="container">
                        <div>
                            <a class="logo" href="javascript:void(0)">
                                <img src="" class="l-dark" height="24" alt="">
                                <img src="" class="l-light" height="24" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="buy-button">
                        <a href="#invest">
                            <div class="btn btn-primary login-btn-primary">Mulai Investasi</div>
                            <div class="btn btn-light login-btn-light">Mulai Investasi</div>
                        </a>
                    </div>
                    <div class="menu-extras">
                        <div class="menu-item">
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div id="navigation">
                        <ul class="navigation-menu nav-light">
                            <li><a href="#produk">Produk Kami</a></li>
                            <li><a href="#bimtek">BIMTEK</a></li>
                            <li><a href="#kerjasama">Kerjasama Investasi</a></li>
                            <li><a href="#galeri">Galeri</a></li>
                        </ul>
                        <div class="buy-menu-btn d-none">
                            <a href="#invest" class="btn btn-primary">Mulai Investasi</a>
                        </div>
                    </div>
                </header>
                 
                <div class="position-relative">
                    <div class="shape overflow-hidden text-footer">
                        <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
                        </svg>
                    </div>
                </div>
                <footer class="footer footer-bar">
                    <div class="container text-center">
                        <div class="row align-items-center">
                            <div class="col-sm-8">
                                <div class="text-sm-left">
                                    <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> . Powered <i class="mdi mdi-heart text-danger"></i> by <a href="http://ptnetindo.com/" target="_blank" class="text-reset">PT NETINDO PERKASA</a>.</p>
                                </div>
                            </div><!--end col-->
                
                            <div class="col-sm-4 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <ul class="list-unstyled text-sm-right mb-0">
                                    <li class="list-inline-item"><a href=""><img style="max-height: 20px!important;" src="" title="" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <script src="'.$url1.'js/jquery-3.5.1.min.js"></script>
                <script src="'.$url1.'js/bootstrap.bundle.min.js"></script>
                <script src="'.$url1.'js/jquery.easing.min.js"></script>
                <script src="'.$url1.'js/scrollspy.min.js"></script>
                <script src="'.$url1.'js/jquery.magnific-popup.min.js"></script>
                <script src="'.$url1.'js/magnific.init.js"></script>
                <script src="'.$url1.'js/owl.carousel.min.js"></script>
                <script src="'.$url1.'js/owl.init.js"></script>
                <script src="'.$url1.'js/jquery.flexslider-min.js"></script>
                <script src="'.$url1.'js/flexslider.init.js"></script>
                <script src="'.$url1.'js/counter.init.js"></script>
                <script src="'.$url1.'js/feather.min.js"></script>
                <script src="'.$url1.'js/bundle.js"></script>
                <script src="'.$url1.'js/switcher.js"></script>
                <script src="'.$url1.'js/app.js"></script>
                <script src="'.$url1.'js/aos.js"></script>
                <link href="'.$url1.'plugins/justified-gallery/css/justifiedGallery.css" type="text/css" media="all" />
                <script src="'.$url1.'plugins/justified-gallery/js/jquery.justifiedGallery.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js" integrity="sha512-+m6t3R87+6LdtYiCzRhC5+E0l4VQ9qIT1H9+t1wmHkMJvvUQNI5MKKb7b08WL4Kgp9K0IBgHDSLCRJk05cFUYg==" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.js" integrity="sha512-/jeu5pizOsnKAR+vn28EbhN5jDSPTTfRzlHZh8qSqB77ckQd3cOyzCG9zo20+O7ZOywiguSe+0ud+8HQMgHH9A==" crossorigin="anonymous"></script>

            </body>
        </html>
        ';
    }


    public function index(){
//        $this->load->dbforge();
////        $this->dbforge->create_database('acuy');
////        if ($this->dbforge->create_database('Library'))
////        {
////            echo 'Database created successfully...';
////        }
//        // switch over to Library DB
//        $this->db->query('use Library');
//
//// define table fields
//        $fields = array(
//            'memid' => array(
//                'type' => 'INT',
//                'constraint' => 9,
//                'unsigned' => TRUE,
//                'auto_increment' => TRUE
//            ),
//            'firstname' => array(
//                'type' => 'VARCHAR',
//                'constraint' => 30
//            ),
//            'lastname' => array(
//                'type' => 'VARCHAR',
//                'constraint' => 30
//            ),
//            'email' => array(
//                'type' => 'VARCHAR',
//                'constraint' => 60,
//                'unique' => TRUE
//            ),
//            'password' => array(
//                'type' => 'VARCHAR',
//                'constraint' => 40
//            )
//        );
//
//        $this->dbforge->add_field($fields);
//
//// define primary key
//        $this->dbforge->add_key('memid', TRUE);
//
//// create table
//        $this->dbforge->create_table('Members');
//        $this->createController('Coba');
        $this->load->library('user_agent');
        $user_ip = $_SERVER['REMOTE_ADDR'];
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Other';
        }
        $cek_ip = $this->m_crud->read_data("visitor", "COUNT(ip_visitor) AS jml", "ip_visitor='".$user_ip."'")->result_array();
        $isExist = $cek_ip[0]['jml'];
        if ($isExist == 0) {
            $this->m_crud->create_data("visitor", array(
                "ip_visitor" => $user_ip,
                "browser_visitor" => $agent
            ));
        }

        $data['page']       = 'home';
        $data['content']    = 'fo/temp1/home';
        $data['setting']     = $this->m_crud->read_data("setting","*")->row_array();
        $data['socmed']     = $this->m_crud->read_data("section","*","type=10")->result_array();
        $data['slider']     = $this->m_crud->read_data("section","*","type=1")->result_array();
        $data['service']    = $this->m_crud->read_data("section","*","type=3")->result_array();
        $data['about']      = $this->m_crud->read_data("section","*","type=2")->row_array();
        $data['product']    = $this->m_crud->read_data("section","*","type=8")->result_array();
        $data['kerjasama']  = $this->m_crud->read_data("section","*","type=7")->result_array();
        $data['workFlow']   = $this->m_crud->read_data("section","*","type=4")->result_array();
        $data['bimtek']     = $this->m_crud->read_data("section","*","type=5")->result_array();
        $data['gallery']    = $this->m_crud->read_data("album","*","type=1")->result_array();
        $data['testimoni']  = $this->m_crud->read_data("section","*","type=6")->result_array();
        $this->load->view("fo/temp1/index",$data);
    }

}