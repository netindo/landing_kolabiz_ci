<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 15:12
 */

class M_crud extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create_data($tabel, $data){
        $data = $this->db->insert($tabel, $data);
        return $data;
    }
    public function read_data($table, $field, $where=null, $order=null, $group=null, $limit_sum=0, $limit_from=0, $having=null){
        $this->db->select($field);
        $this->db->from($table);
        if($where != null){ $this->db->where($where); }
        if($order != null){ $this->db->order_by($order); }
        if($group != null){ $this->db->group_by($group); }
        if($having != null){ $this->db->having($having); }
        if($limit_sum != 0){ $this->db->limit($limit_sum, $limit_from); }
        $data = $this->db->get();
        return $data;
    }
    public function update_data($tabel, $data, $where){
        $data = $this->db->update($tabel, $data, $where);
        return $data;
    }
    public function delete_data($tabel, $where){
        $data = $this->db->delete($tabel, $where);
        return $data;
    }
    public function count_data_join($table, $field, $table_join, $on, $where=null, $order=null, $group=null, $limit_sum=0, $limit_from=0, $having=null, $distinct=''){
        $col = explode('.', $field);
        if (count($col) > 1) {
            $alias = $col[1];
        } else {
            $alias = $field;
        }
        $this->db->select("COUNT(".$distinct." ".$field.") AS ".$alias."");
        $this->db->from($table);
        if(is_array($table_join) && is_array($on)){
            $i = 0;
            foreach($table_join as $row){
                if (is_array($row)) {
                    $this->db->join($row['table'], $on[$i], $row['type']);
                } else {
                    $this->db->join($row, $on[$i]);
                }
                $i++;
            }
        } else {
            $this->db->join($table_join, $on);
        }
        if($where != null){ $this->db->where($where); }
        if($order != null){ $this->db->order_by($order); }
        if($group != null){ $this->db->group_by($group); }
        if($having != null){ $this->db->having($having); }
        if($limit_sum != 0){ $this->db->limit($limit_sum, $limit_from); }
        $data = $this->db->get();
        foreach ($data->result_array() as $row);
        return $row[$alias];
    }

    public function count_data($table, $field, $where=null, $order=null, $group=null, $limit_sum=0, $limit_from=0, $having=null, $distinct=''){
        $col = explode('.', $field);
        if (count($col) > 1) {
            $alias = $col[1];
        } else {
            $alias = $field;
        }
        $this->db->select("COUNT(".$distinct." ".$field.") AS ".$alias."");
        $this->db->from($table);
        if($where != null){ $this->db->where($where); }
        if($order != null){ $this->db->order_by($order); }
        if($group != null){ $this->db->group_by($group); }
        if($having != null){ $this->db->having($having); }
        if($limit_sum != 0){ $this->db->limit($limit_sum, $limit_from); }
        $data = $this->db->get();
        foreach ($data->result_array() as $row);
        return $row[$alias];
    }

    public function myPagination($param,$uri,$table,$field,$join=null,$on=null,$where,$perPage,$page){
        if($param == 'join'){
            $count =$this->count_data_join($table, $field, $join, $on, $where);
        }else{
            $count = $this->count_data($table, $field, $where);
        }
        $config = array();
        $config["base_url"] 		= "javascript:void(0)";
        $config["total_rows"] 		= $count;
        $config["per_page"] 		= $perPage;
        $config["uri_segment"] 		= $uri;
        $config["num_links"] 		= 2;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] 	= /** @lang text */'<ul class="pagination pagination-sm">';
        $config["full_tag_close"] 	= /** @lang text */'</ul>';
        $config['first_link'] 		= '&laquo;';
        $config["first_tag_open"] 	= /** @lang text */'<li>';
        $config["first_tag_close"] 	= /** @lang text */'</li>';
        $config['last_link'] 		= '&raquo;';
        $config["last_tag_open"] 	= /** @lang text */'<li>';
        $config["last_tag_close"] 	= /** @lang text */'</li>';
        $config['next_link'] 		= '&gt;';
        $config["next_tag_open"] 	= /** @lang text */'<li>';
        $config["next_tag_close"] 	= /** @lang text */'</li>';
        $config["prev_link"] 		= "&lt;";
        $config["prev_tag_open"] 	= /** @lang text */"<li>";
        $config["prev_tag_close"] 	= /** @lang text */"</li>";
        $config["cur_tag_open"] 	= /** @lang text */"<li class='active'><a href='#'>";
        $config["cur_tag_close"] 	= /** @lang text */"</a></li>";
        $config["num_tag_open"] 	= /** @lang text */"<li>";
        $config["num_tag_close"] 	= /** @lang text */"</li>";
        $this->pagination->initialize($config);
        return $data = array(
            'start' => ($page - 1) * $config["per_page"],
            'per_page' => $config['per_page'],
            'pagination_link' => $this->pagination->create_links()
        );
    }


}