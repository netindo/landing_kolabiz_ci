<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 18/01/2021
 * Time: 12:42
 */
?>



<div class="col-12">
    <div class="row" id="result">
    </div>
</div>


<div class="modal inmodal" id="modal_form" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_input">
                <div class="modal-body">
                    <div class="form-group">
                        <?php $label='title'?>
                        <label>Title</label>
                        <input type="text" class="form-control" name="<?=$label?>" id="<?=$label?>">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link_video'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group">
                        <?php $label='description'?>
                        <label>Description</label>
                        <textarea class="form-control" name="<?=$label?>" id="<?=$label?>" style="height: 146px;"></textarea>
                    </div>

                    <div class="form-group">
                        <?php $label = 'file_upload'; ?>
                        <label>Thumbnail</label>
                        <input type="hidden" id="file_uploaded" name="file_uploaded" />
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="file" id="<?=$label?>" name="<?=$label?>" onchange="return ValidateFileUpload()" accept="image/*">
                                <p class="error" id="alr_<?=$label?>"></p>
                            </div>
                            <div class="col-sm-3">
                                <img style="width: 100%;height: auto;" src="<?=base_url().'assets/images/no_image.png'?>" id="result_image">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                <input type="hidden" name="param" id="param" value="add">
                <input type="hidden" name="type" id="type" value="2">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="page" id="page" value="<?=$page?>">
            </form>
        </div>
    </div>
</div>

<script>
    load_data()
    function load_data() {
        $.ajax({
            url:"<?=base_url().'bo/about/load_data/';?>",
            type:"GET",
            dataType:"JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success:function(data)
            {
                $('#result').html(data.result);
            }
        });
    }
    function edit(id) {
        $.ajax({
            url: "<?=base_url().'bo/section/edit'?>",
            type: "POST",
            data: {id: id},
            dataType: "JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success: function (res) {
                if (res.status) {
                    var page=$("#page").val();
                    $("#modal_title").text("Edit Slider");
                    $("#param").val("edit");
                    $("#id").val(id);
                    $("#title").val(res.result['title']);
                    $("#description").val(res.result['description']);
                    $("#link").val(res.result['link']);
                    $("#link_video").val(res.result['link_video']);
                    $("#modal_form").modal("show");
                    if(page==='service'||page==='about'){
                        $('#file_uploaded').val((res.result['image']!=''?res.result['image']:''));
                        $('#result_image').attr('src', '<?= base_url() ?>' + (res.result['image']!=''?res.result['image']:'assets/images/no_image.png'));
                    }
                    else{
                        $("#image").val(res.result['image'])
                    }

                    setTimeout(function () {
                        $("#title").focus();
                    }, 600);
                } else {
                    alert("Error getting data!")
                }
            }
        });
    }
    $('#form_input').validate({
        rules: {
            title: {required: true},
            description: {required: true},
        },
        messages: {
            title:{required: "title cannot be empty"},
            description:{required: "description cannot be empty"},
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_input');
            $.ajax({
                url: "<?=base_url().'bo/section/save'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data(1);
                    $("#modal_form").modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Berhasil Disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });
        }
    });

    $("#modal_form").on("hide.bs.modal", function () {
        document.getElementById("form_input").reset();
        $( "#form_input" ).validate().resetForm();
        $('#result_image').attr('src', '<?= base_url() ?>' + 'assets/images/no_image.png');

    });
    function ValidateFileUpload() {
        var fuData = document.getElementById('file_upload');
        var FileUploadPath = fuData.value;
        var valid = 1;
        $("#alr_file_upload").text("");
        if (FileUploadPath == '') {
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg"|| Extension == "svg") {
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#result_image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(fuData.files[0]);
                }
            }
        }
        return valid;
    }


</script>