<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 19/01/2021
 * Time: 16:25
 */
?>

<!doctype html>
<html lang="en">


<!-- Mirrored from demo.riktheme.com/motrila-2/side-menu/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Jan 2020 17:30:50 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title><?=$setting['name']?></title>

    <!-- Favicon -->
    <link rel="icon" href="<?=base_url().$setting['logo']?>">

    <!-- Master Stylesheet [If you remove this CSS file, your file will be broken undoubtedly.] -->
    <link rel="stylesheet" href="<?=base_url().'assets/bo/'?>style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Edu+SA+Beginner&family=Signika+Negative&display=swap" rel="stylesheet">
    <style>
        .error{color:red!important;font-weight: bold!important;}
        ul,li,h1,h2,h3,h4,h5,h6,p,a,tr,table,td,th,span,input,textarea,.jg-caption,html,body{
            font-family: 'Signika Negative', sans-serif;
        }
        .first-loader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1050;
            background: rgba(168, 168, 168, .5)
        }
        .first-loader img {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            width: 60px;
            height: 60px
        }
    </style>
</head>

<body class="login-area">



<!-- ======================================
******* Page Wrapper Area Start **********
======================================= -->
<div class="main-content- h-100vh">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12">
                <!-- Middle Box -->
                <div class="middle-box">
                    <div class="card">
                        <div class="card-body p-4">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <div class="xs-d-none mb-50-xs break-320-576-none">
                                        <img src="<?=base_url().'assets/bo/'?>img/bg-img/1.png" alt="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Logo -->
                                    <h4 class="font-18 mb-30">Welcome back! Log in to your account.</h4>
                                    <form id="form_account">
                                        <div class="form-group">
                                            <?php $label='username';?>
                                            <label class="float-left" for="emailaddress">Username</label>
                                            <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        </div>
                                        <div class="form-group">
                                            <?php $label='password';?>
                                            <label class="float-left" for="password">Password</label>
                                            <input class="form-control" type="password" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        </div>

                                        <div class="form-group mb-0">
                                            <button class="btn btn-primary btn-block" type="submit"> Log In </button>
                                        </div>
                                    </form>
                                </div> <!-- end card-body -->
                            </div>
                            <!-- end card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ======================================
********* Page Wrapper Area End ***********
======================================= -->

<!-- Must needed plugins to the run this Template -->
<script src="<?=base_url().'assets/bo/'?>js/jquery.min.js"></script>
<script src="<?=base_url().'assets/bo/'?>js/popper.min.js"></script>
<script src="<?=base_url().'assets/bo/'?>js/bootstrap.min.js"></script>
<script src="<?=base_url().'assets/bo/'?>js/bundle.js"></script>

<!-- Active JS -->
<script src="<?=base_url().'assets/bo/'?>js/default-assets/active.js"></script>
<!-- Form Validation -->
<script src="<?=base_url().'assets/'?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=base_url().'assets/'?>plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    $(document).ready(function(){
        $("#username").focus();
    })
    $('#form_account').validate({
        rules: {
            username: {required: true},
            password: {required: true}
        },
        messages: {
            username:{required: "username cannot be empty"},
            password:{required: "password cannot be empty"}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_account');
            $.ajax({
                url: "<?=base_url().'auth/login'?>",
                type: "POST",
                data: new FormData(myForm),
                dataType:"JSON",
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    if(res.status===true){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'redirect to dashboard',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        setTimeout(function(){ window.location.href="<?=base_url().'bo'?>"; }, 1500);



                    }
                    else{
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: res.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }

                }
            });
        }
    });

</script>

<!-- Mirrored from demo.riktheme.com/motrila-2/side-menu/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 03 Jan 2020 17:30:50 GMT -->
</html>
