<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 14:58
 */
?>

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<style>
    #chart{z-index:-10;}
    .bg-info{background-color: #000000!important;color#fff!important;}
    .bx-shadow{padding: 10px;}
</style>
<?php
$getTahun = $this->db->query("SELECT YEAR(date_visitor) AS 'thn' FROM visitor  GROUP BY DATE_FORMAT(date_visitor,'%y') ORDER BY YEAR(date_visitor) DESC")->result();
?>
<!-----------------------------------ORIGINAL------------------------------------->

<div class="col-12 box-margin">
    <div class="row">
        <div class="col-md-6 box-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <select name="tahun" id="tahun" class="form-control" onchange="handleYear()">
                                <option value="">Select Year</option>
                                <?php foreach($getTahun as $row):?>
                                    <option value="<?=$row->thn?>"><?=$row->thn?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select name="bulan" id="bulan" class="form-control" onchange='handleMonth()'></select>
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            <div id="container_hari" class="col-md-12" style="padding: 0px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 box-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <select name="tahun" id="filter" class="form-control" onchange="load_month()">
                                <?php foreach($getTahun as $row):?>
                                    <option value="<?=$row->thn?>"><?=$row->thn?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            <div id="container_bulan"class="col-md-12"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 box-margin">
            <div class="card">
                <div class="card-body">
                    <div id="container_tahun"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6 box-margin">
            <div class="card">
                <div class="card-body">
                    <div id="device"></div>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
    var setting = localStorage.companyName;

    var img_url = "<?=base_url().'assets/images/'?>";
    $(document).ready(function(){
        handleMonth()
        load_month()
        filter_by_year()
        handleYear()
        load_device()
    })

    function load_device(){
        $.ajax({
            url       : "<?=base_url().'bo/dashboard/get'?>",
            type      : "POST",
            dataType  : "JSON",
            beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
            complete: function() {$('.first-loader').remove();},
            success   : function(res){
                Highcharts.chart('device', {
                    chart       : { type: 'column',options3d: {enabled: true, alpha: 10,beta: 0,depth: 70},zoomType: 'xy'},
                    title       : { text: 'Graph By Browser'},
                    subtitle    : { text: '<a href="<?=base_url()?>">'+setting+'</a>'},
                    plotOptions : { column: {depth: 25}},
                    credits     : { enabled: false},
                    xAxis       : { categories: res.perangkat,labels  : {skew3d: true,style : {fontSize: '10px'}}},
                    yAxis       : { title: {text: null},scrollbar: {enabled: true,showFull: false}},
                    series      : [{name: 'Browser',data: res.jumlah}]
                });
            }
        })
    }



    function handleYear(){
        var thn     = $("#tahun").val();
        var html = "";

        if(thn==""){
            $("#bulan").prop("readonly",true);
        }else {
            $("#bulan").prop("readonly", false);
            var filter = {"tahun": thn}
            $.ajax({
                url: "<?=base_url() . 'bo/dashboard/month'?>",
                type: "POST",
                dataType: "JSON",
                data: filter,
                beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
                complete: function() {$('.first-loader').remove();},
                success: function (res) {
                    var optId = res.grafik
                    var html = "";
                    html += "<option value=''>Select Month</option>";
                    for (var i = 0; i < optId.length; i++) {
                        html += "<option value='" + optId[i].valbulan + "'>" + optId[i].bulan + "</option>";
                    }
                    $("#bulan").html(html);


                }
            })
        }
    }



    function handleMonth() {
        var thn     = $("#tahun").val();
        var bln     = $("#bulan").val();
        var filter  = {"tahun" : thn,"bulan" : bln}
        $.ajax({
            url       : "<?=base_url() . 'bo/dashboard/hari'?>",
            type      : "POST",
            dataType  : "JSON",
            data      : filter,
            beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
            complete: function() {$('.first-loader').remove();},
            success   : function (res) {
                Highcharts.chart('container_hari', {
                    chart: {type: 'column', options3d: {enabled: true, alpha: 10, beta: 0, depth: 70}, zoomType: 'xy'},
                    title: {text: 'Graph By Day'},
                    subtitle: {text: '<a href="<?=base_url()?>">' + setting + '</a>'},
                    plotOptions: {column: {depth: 25}},
                    credits: {enabled: false},
                    xAxis: {categories: res.tgl, labels: {skew3d: true, style: {fontSize: '10px'}}},
                    yAxis: {title: {text: null}, scrollbar: {enabled: true, showFull: true}},
                    series: [{name: 'Visitor', data: res.jumlah}]
                });
            }
        })
    }
    function load_month() {
        var param = $("#filter").val();
        var filter = {"tahun" : param}
        $.ajax({
            url       : "<?=base_url() . 'bo/dashboard/month'?>",
            type      : "POST",
            dataType  : "JSON",
            data      : filter,
            beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
            complete: function() {$('.first-loader').remove();},
            success   : function (res) {
                Highcharts.chart('container_bulan', {
                    chart: {type: 'spline', options3d: {enabled: true, alpha: 10, beta: 0, depth: 70}, zoomType: 'xy'},
                    title: {text: 'Graph By Month'},
                    subtitle: {text: '<a href="<?=base_url()?>">' + setting + '</a>'},
                    plotOptions: {column: {depth: 25}},
                    credits: {enabled: false},
                    xAxis: {categories: res.bln, labels: {skew3d: true, style: {fontSize: '10px'}}},
                    yAxis: {title: {text: null}, scrollbar: {enabled: true, showFull: false}},
                    series: [{name: 'Visitor', data: res.jml}]
                });
            }
        })
    }
    function filter_by_year() {
        $.ajax({
            url       : "<?=base_url() . 'bo/dashboard/year'?>",
            type      : "POST",
            dataType  : "JSON",
            beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
            complete: function() {$('.first-loader').remove();},
            success   : function (res) {
                Highcharts.chart('container_tahun', {
                    chart       : { type: 'area',options3d: {enabled: true, alpha: 10,beta: 0,depth: 70},zoomType: 'xy'},
                    title       : { text: 'Graph By Year'},
                    subtitle    : { text: '<a href="<?=base_url()?>">'+setting+'</a>'},
                    plotOptions : {
                        area: {
                            marker    : {
                                enabled : false,symbol: 'circle',radius: 2,
                                states  : {hover : {enabled: true}}
                            }
                        }
                    },
                    credits     : { enabled: false},
                    xAxis       : { categories: res.year,labels    : {skew3d: true,style : {fontSize: '10px'}}},
                    yAxis       : { title: {text: null}},
                    series      : [{name: 'Visitor',data: res.total}]
                });
            }
        })
    }


</script>

