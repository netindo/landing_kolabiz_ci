<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 18/01/2021
 * Time: 20:52
 */
?>



<div class="col-12 box-margin">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="any" name="any" placeholder="tulis sesuatu disini dan tekan enter" onkeyup="return cari(event, $(this).val())">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" type="button" onclick="add('edit')"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-12 box-margin">
    <div class="card">
        <div class="card-body" style="padding: 0px;" id="result">
        </div>
    </div>
</div>
<div class="modal inmodal" id="modal_form" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_input" enctype="multipart/form-data">
                <div class="modal-body">
                    <button class="btn btn-primary" onclick="changeRow(event,'add')"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-danger" onclick="changeRow(event,'remove')"><i class="fa fa-close"></i></button>
                    <br/>
                    <div class="table-responsive" style="margin-top: 10px">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody id="res_row">

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                <input type="hidden" name="param" id="param" value="add">
                <input type="hidden" name="type" id="type" value="1">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="list" id="list">
                <input type="hidden" name="page" id="page" value="<?=$page?>">
            </form>
        </div>
    </div>
</div>
<style>
    .mfp-s-loading .mfp-arrow {
        display: none;
    }
</style>
<script>



    var x=1;
    function hapus(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(function(result){
            if(result.isConfirmed){
                $.ajax({
                    url: "<?=base_url().'bo/album/hapus'?>",
                    type: "POST",
                    data: {id: id},
                    dataType: "JSON",
                    beforeSend: function() {
                        $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                    },
                    complete: function() {
                        $('.first-loader').remove();
                    },
                    success: function (res) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Data Berhasil Dihapus',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        load_data();
                    },
                    error: function(xhr, status, error) {
                        alert("Data tidak bisa dihapus!");
                    }
                });

            }
            else if (result.dismiss === Swal.DismissReason.cancel) {
                result.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })

    }
    $(document).ready(function(){
        load_data();
    }).on("click", ".pagination li a", function(event){
        event.preventDefault();
        var page = $(this).data("ci-pagination-page");
        load_data();
    });



    function load_data() {
        $.ajax({
            url:"<?=base_url().'bo/gallery/load_data/';?>",
            type:"POST",
            dataType:"JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success:function(data)
            {
                var res='';
                $.each(data.result, function (index, value) {
                    var url = '<?=base_url()?>'+value['image'];
                    res+='<a href="javascript:void(0)" onclick="hapus(\''+value["id"]+'\')" title="'+value["title"]+'">';
                    res+='<img style="width: 100%" src="'+url+'" style="width: 100%">';
                    res+='</a>';
                });
                $("#result").html(res);
                $('#result').justifiedGallery({rowHeight:150});
            }
        });
    }


    function changeRow(e,param){
        e.preventDefault();
        if(param==='add'){
            x+=1;
            col();
        }else{
            if(x>1){
                x-=1;
                col();
            }
        }
        $("#list").val(x);
    }




    function add() {
        $("#modal_title").text("Add Gallery");
        $("#param").val("add");
        $("#modal_form").modal("show");
        $("#list").val(x);
        setTimeout(function () {
            $("#title").focus();
        }, 600);
        col();
    }

    function col(){
        var res = '';
        var z=0;
        for(var i=0;i<x;i++){
            z++;
            res+='<tr>\n' +
                '<td>'+z+'</td>\n' +
                '<td><input type="text" name="title[]"  id="title'+i+'" class="form-control"></td>\n' +
                '<td><input type="file" name="image[]" id="image'+i+'" class="form-control"></td>\n' +
            '</tr>';
        }
        $("#res_row").html(res);
    }

    $("#form_input").submit(function(e){
        e.preventDefault();
        for(var i=0;i<x;i++){
            if($("#title"+i).val()===""){
                $("#title"+i).focus();
                return false;
            }
            if($("#image"+i).val()===""){
                $("#image"+i).focus();
                return false;
            }
        }
        var myForm = document.getElementById('form_input');
        $.ajax({
            url: "<?=base_url().'bo/album/save'?>",
            type: 'POST',
            dataType : "JSON",
            data: new FormData(myForm),
            mimeType: "multipart/form-data",
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success: function (res) {
                load_data();
                if(res.pesan===''){
                    $("#modal_form").modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Berhasil Disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                else{
                    alert("gagal");
                }

            }
        });
    })

    $("#modal_form").on("hide.bs.modal", function () {
        document.getElementById("form_input").reset();
        x=1;
    });
</script>
