<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 14:44
 */

$url=base_url().'assets/bo/';
$setting=$this->m_crud->read_data("setting","*")->row_array();
?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title id="titleWeb"><?=$setting['name']?></title>
    <link id="favicon" rel="shortcut icon" type="image/png" href="<?=base_url().$setting['logo']?>" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Edu+SA+Beginner&family=Signika+Negative&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?=$url?>css/c3.min.css">
    <link rel="stylesheet" href="<?=base_url().'assets/'?>css/line.css">
    <link rel="stylesheet" href="<?=$url?>style.css">
    <link rel='stylesheet' href="<?=base_url().'assets/plugins/justified-gallery/css/justifiedGallery.css'?>" type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.css" integrity="sha512-4a1cMhe/aUH16AEYAveWIJFFyebDjy5LQXr/J/08dc0btKQynlrwcmLrij0Hje8EWF6ToHCEAllhvGYaZqm+OA==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" integrity="sha512-nIm/JGUwrzblLex/meoxJSPdAKQOe2bLhnrZ81g5Jbh519z8GFJIWu87WAhBH+RAyGbM4+U3S2h+kL5JoV6/wA==" crossorigin="anonymous" />


    <script src="<?=$url?>js/jquery.min.js"></script>
    <script src="<?=$url?>js/popper.min.js"></script>
    <script src="<?=$url?>js/bootstrap.min.js"></script>
    <script src="<?=$url?>js/default-assets/bootstrap-growl.js"></script>
    <script src="<?=$url?>js/default-assets/date-time.js"></script>
    <!-- Form Validation -->
    <script src="<?=base_url().'assets/'?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=base_url().'assets/'?>plugins/jquery-validation/additional-methods.min.js"></script>
    <!-- Sweet Alert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- GALLERY IMAGE HERE -->
    <script type='text/javascript' src='<?=base_url().'assets/plugins/justified-gallery/js/jquery.justifiedGallery.js'?>'></script>
    <script type='text/javascript' src='<?=base_url().'assets/plugins/justified-gallery/js/jquery.justifiedGallery.js'?>'></script>
    <script src="<?=base_url('assets/plugins/ckeditor/ckeditor.js');?>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js" integrity="sha512-+m6t3R87+6LdtYiCzRhC5+E0l4VQ9qIT1H9+t1wmHkMJvvUQNI5MKKb7b08WL4Kgp9K0IBgHDSLCRJk05cFUYg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.js" integrity="sha512-/jeu5pizOsnKAR+vn28EbhN5jDSPTTfRzlHZh8qSqB77ckQd3cOyzCG9zo20+O7ZOywiguSe+0ud+8HQMgHH9A==" crossorigin="anonymous"></script>

    <style>

        ul,li,h1,h2,h3,h4,h5,h6,p,a,tr,table,td,th,span,input,textarea,.jg-caption,html,body{
            font-family: 'Signika Negative', sans-serif;
        }
        .error{
            color:red;
        }
        .first-loader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1050;
            background: rgba(168, 168, 168, .5)
        }
        .first-loader img {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            width: 60px;
            height: 60px
        }
        .pagination > li > a {
            padding: 6px 12px;
            color: #000000;
            font-size: 16px;
            font-weight: bold;
            background-color: #ffffff;
            border: 1px solid #dddddd;
            margin: 0 3px;
        }
        .pagination > .active > a {
            color: #000000;
            font-weight: bold;
            background-color: #ffffff;
            border: 1px solid #dddddd;
        }
        .pagination > li:first-child > a {
            border-bottom-left-radius: 0;
            border-top-left-radius: 0;
        }
        .pagination > li:last-child > a {
            border-bottom-right-radius: 0;
            border-top-right-radius: 0;
        }
        .pagination > li > a:hover,
        .pagination > li > a:focus {
            color: #ffffff;
            background-color: #5d78ff;
            border-color: #5d78ff;
        }
        .pagination > .active > a,
        .pagination > .active > a:hover,
        .pagination > .active > a:focus {
            color: #ffffff;
            background-color: #5d78ff;
            border-color: #5d78ff;
            z-index: -0!important;
        }
        .pagination > .disabled > a,
        .pagination > .disabled > a:hover,
        .pagination > .disabled > a:focus {
            color: #777777;
            background-color: #ffffff;
            border-color: #dddddd;

        }
    </style>

    <script>
        localStorage.setItem("baseUrl","<?=base_url()?>");
        $(window).load(function () {
            loadSetting();
        });
        function loadSetting(){
            $.ajax({
                url:"<?=base_url().'bo/setting/load_data/';?>",
                type:"GET",
                dataType:"JSON",
                success:function(data)
                {
//                    $('head').append('<link href="'+data.setting.logo+'" rel="shortcut icon" type="image/x-icon" />');
                    $("#favicon").attr("href","<?=base_url()?>"+data.setting.logo);
                    $("#companyName").text(data.setting.name);
                    $("#titleWeb").text(data.setting.name);
                    localStorage.setItem("companyName",data.setting.name);
                    $('#desktopLogo').attr('src', '<?= base_url() ?>' + (data.setting.logo!=''?data.setting.logo:'assets/images/no_image.png'));
                    $('#mobileLogo').attr('src', '<?= base_url() ?>' + (data.setting.logo!=''?data.setting.logo:'assets/images/no_image.png'));
                }
            });
        }
    </script>
</head>

<body class="sidebar-dark">


<!-- ======================================
******* Page Wrapper Area Start **********
======================================= -->
<div class="ecaps-page-wrapper">
    <!-- Sidemenu Area -->
    <div class="ecaps-sidemenu-area">
        <!-- Desktop Logo -->
        <div class="ecaps-logo">
            <a href="javascript:void(0)">
                <img id="desktopLogo" class="desktop-logo"  alt="Desktop Logo" src="<?=base_url().$setting['logo']?>">
                <img id="mobileLogo" class="small-logo" src="" alt="Mobile Logo" src="<?=base_url().$setting['logo']?>">
            </a>
        </div>

        <!-- Side Nav -->
        <div class="ecaps-sidenav" id="ecapsSideNav">
            <!-- Side Menu Area -->
            <div class="side-menu-area">
                <!-- Sidebar Menu -->
                <nav>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="<?=$page=='dashboard'?'active':null?>"><a href="<?=base_url().'bo'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Dashboard</span></a></li>
                        <li class="<?=$page=='setting'?'active':null?>"><a href="<?=base_url().'bo/setting'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Setting</span></a></li>
                        <li class="<?=$page=='slider'?'active':null?>"><a href="<?=base_url().'bo/slider'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Slider</span></a></li>
                        <li class="<?=$page=='about'?'active':null?>"><a href="<?=base_url().'bo/about'?>"><i class="zmdi zmdi-view-dashboard"></i><span>About</span></a></li>
                        <li class="<?=$page=='service'?'active':null?>"><a href="<?=base_url().'bo/service'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Service</span></a></li>
                        <li class="<?=$page=='rules'?'active':null?>"><a href="<?=base_url().'bo/rules'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Rules</span></a></li>
                        <li class="<?=$page=='fitur_tambahan'?'active':null?>"><a href="<?=base_url().'bo/bimtek'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Fitur Tambahan</span></a></li>
                        <li class="<?=$page=='testimoni'?'active':null?>"><a href="<?=base_url().'bo/testimoni'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Testimoni</span></a></li>
                        <li class="<?=$page=='kerjasama'?'active':null?>"><a href="<?=base_url().'bo/kerjasama'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Kerjasama</span></a></li>
                        <li class="<?=$page=='product'?'active':null?>"><a href="<?=base_url().'bo/product'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Product</span></a></li>
                        <li class="<?=$page=='gallery'?'active':null?>"><a href="<?=base_url().'bo/gallery'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Gallery</span></a></li>
                        <li class="<?=$page=='socmed'?'active':null?>"><a href="<?=base_url().'bo/socmed'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Socmed</span></a></li>
                        <li><a href="<?=base_url().'auth/logout'?>"><i class="zmdi zmdi-view-dashboard"></i><span>Logout</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <!-- Page Content -->
    <div class="ecaps-page-content">
        <!-- Top Header Area -->
        <header class="top-header-area d-flex align-items-center justify-content-between">
            <div class="left-side-content-area d-flex align-items-center">
                <!-- Mobile Logo -->
                <div class="mobile-logo mr-3 mr-sm-4">
                    <a href="javascript:void(0)"><img src="<?=$url?>img/core-img/small-logo.png" alt="Mobile Logo"></a>
                </div>
                <!-- Triggers -->
                <div class="ecaps-triggers mr-1 mr-sm-3">
                    <div class="menu-collasped" id="menuCollasped">
                        <i class="zmdi zmdi-menu"></i>
                    </div>
                    <div class="mobile-menu-open" id="mobileMenuOpen">
                        <i class="zmdi zmdi-menu"></i>
                    </div>
                </div>
            </div>
            <div class="right-side-navbar d-flex align-items-center justify-content-end">
                <!-- Mobile Trigger -->
                <div class="right-side-trigger" id="rightSideTrigger">
                    <i class="ti-align-left"></i>
                </div>

                <!-- Top Bar Nav -->
                <ul class="right-side-content d-flex align-items-center">
                    <!-- Full Screen Mode -->
                    <li class="full-screen-mode">
                        <a href="<?=base_url('auth/logout')?>" id="fullScreenMode"><i class="fa fa-sign-out"></i></a>
                    </li>
                </ul>
            </div>
        </header>

        <!-- Main Content Area -->
        <div class="main-content">
            <div class="dashboard-area">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-6">
                            <div class="dashboard-header-title mb-3">
                                <h5 class="mb-0 font-weight-bold text-primary" style="float: left"> <b id="companyName"><?=$setting['name']?></b> / <b><?=str_replace('_',' ',strtoupper($page))?></b> </h5>
                            </div>
                        </div>
                        <!-- Dashboard Info Area -->
                        <div class="col-6">
                            <div class="dashboard-infor-mation d-flex flex-wrap align-items-center mb-3">
                                <div class="dashboard-clock">
                                    <div id="dashboardDate">
                                        Monday, 21 October
                                    </div>
                                    <ul class="d-flex align-items-center justify-content-end">
                                        <li id="hours">12</li>
                                        <li>:</li>
                                        <li id="min">10</li>
                                        <li>:</li>
                                        <li id="sec">14</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php $this->load->view('bo/'.$content);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ======================================
********* Page Wrapper Area End ***********
======================================= -->

<!-- Must needed plugins to the run this Template -->

<script src="<?=$url?>js/bundle.js"></script>
<!-- Active JS -->
<script src="<?=$url?>js/default-assets/active.js"></script>
<script src="<?=$url?>js/default-assets/modal-classes.js"></script>
<script src="<?=$url?>js/default-assets/modaleffects.js"></script>

</html>
