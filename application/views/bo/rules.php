<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 18/01/2021
 * Time: 15:07
 */
?>

<div class="col-12 box-margin">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="any" name="any" placeholder="tulis sesuatu disini dan tekan enter" onkeyup="return cari(event, $(this).val())">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" type="button" onclick="add()"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="row" id="result" style="margin-bottom: 10px">
    </div>
</div>

<div class="col-12">
    <div class="row">
        <div class="col-md-12">
            <div id="pagination_link" style="float: right;"></div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal_form" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_input">
                <div class="modal-body">
                    <div class="form-group">
                        <?php $label='title'?>
                        <label>Title</label>
                        <input type="text" class="form-control" name="<?=$label?>" id="<?=$label?>">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link_video'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group">
                        <?php $label='description'?>
                        <label>Description</label>
                        <textarea class="form-control" name="<?=$label?>" id="<?=$label?>" style="height: 146px;"></textarea>
                    </div>

                    <div class="form-group">
                        <?php $label = 'file_upload'; ?>
                        <label>Thumbnail</label>
                        <input type="hidden" id="file_uploaded" name="file_uploaded" />
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="file" id="<?=$label?>" name="<?=$label?>" onchange="return ValidateFileUpload()" accept="image/*">
                                <p class="error" id="alr_<?=$label?>"></p>
                            </div>
                            <div class="col-sm-3">
                                <img style="width: 100%;height: auto;" src="<?=base_url().'assets/images/no_image.png'?>" id="result_image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                <input type="hidden" name="param" id="param" value="add">
                <input type="hidden" name="type" id="type" value="4">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="page" id="page" value="<?=$page?>">

            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        load_data(1);
    }).on("click", ".pagination li a", function(event){
        event.preventDefault();
        var page = $(this).data("ci-pagination-page");
        load_data(page);
    });
    function cari(e, val) {
        if (e.keyCode === 13) {
            load_data(1, {search:true, any:val});
        }

    }


    function load_data(page,data={}) {
        $.ajax({
            url:"<?=base_url().'bo/rules/load_data/';?>"+page,
            type:"POST",
            data:data,
            dataType:"JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success:function(data)
            {
                $('#pagination_link').html(data.pagination_link);
                $('#result').html(data.result);
            }
        });
    }
    function add() {
        $("#modal_title").text("Add Rules");
        $("#param").val("add");
        $("#modal_form").modal("show");
        setTimeout(function () {
            $("#title").focus();
        }, 600);
    }
    function edit(id) {
        $.ajax({
            url: "<?=base_url().'bo/section/edit'?>",
            type: "POST",
            data: {id: id},
            dataType: "JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success: function (res) {
                if (res.status) {
                    $("#modal_title").text("Edit Rules");
                    $("#param").val("edit");
                    $("#id").val(id);
                    $("#title").val(res.result['title']);
                    $("#description").val(res.result['description']);
                    $("#link").val(res.result['link']);
                    $("#modal_form").modal("show");
                    $('#file_uploaded').val((res.result['image']!=''?res.result['image']:''));
                    $('#result_image').attr('src', '<?= base_url() ?>' + (res.result['image']!=''?res.result['image']:'assets/images/no_image.png'));

                    setTimeout(function () {
                        $("#title").focus();
                    }, 600);
                } else {
                    alert("Error getting data!")
                }
            }
        });
    }
    function hapus(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(function(result){
            if(result.isConfirmed){
                $.ajax({
                    url: "<?=base_url().'bo/section/hapus'?>",
                    type: "POST",
                    data: {id: id},
                    beforeSend: function() {
                        $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                    },
                    complete: function() {
                        $('.first-loader').remove();
                    },
                    success: function (res) {
                        if(res){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Data Berhasil Dihapus',
                                showConfirmButton: false,
                                timer: 1500
                            })
//                            Swal.fire(
//                                'Deleted!',
//                                'Your file has been deleted.',
//                                'success'
//                            )
                            load_data(1);
                        }
                        else{
                            alert("Data tidak bisa dihapus!");
                        }
                    },
                    error: function(xhr, status, error) {
                        alert("Data tidak bisa dihapus!");
                    }
                });

            }
            else if (result.dismiss === Swal.DismissReason.cancel) {
                result.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })

    }
    $('#form_input').validate({
        rules: {
            title: {required: true},
            description: {required: true}
        },
        messages: {
            title:{required: "title cannot be empty"},
            description:{required: "description cannot be empty"}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_input');
            $.ajax({
                url: "<?=base_url().'bo/section/save'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data(1);
                    $("#modal_form").modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
//                    if (res) {
//                        $("#modal_bank").modal('hide');
//                        load_data(1);
//                    } else {
//                        alert("Data gagal disimpan!");
//                    }
                }
            });
        }
    });
    $("#modal_form").on("hide.bs.modal", function () {
        document.getElementById("form_input").reset();
        $( "#form_input" ).validate().resetForm();
        $('#result_image').attr('src', '<?= base_url() ?>' + 'assets/images/no_image.png');

    });
    function ValidateFileUpload() {
        var fuData = document.getElementById('file_upload');
        var FileUploadPath = fuData.value;
        var valid = 1;
        $("#alr_file_upload").text("");
        if (FileUploadPath == '') {
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg"|| Extension == "svg") {
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#result_image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(fuData.files[0]);
                }
            }
        }
        return valid;
    }


</script>
