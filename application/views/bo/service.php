<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 18/01/2021
 * Time: 13:28
 */
?>


<div class="col-12 box-margin">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="any" name="any" placeholder="tulis sesuatu disini dan tekan enter" onkeyup="return cari(event, $(this).val())">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary" type="button" onclick="add()"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>#</th>
                                    <th>Icon</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody id="result"></tbody>
                        </table>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </div><!--end section-->
</div>

<div class="col-12" style="margin-top: 10px">
    <div class="row">
        <div class="col-md-12">
            <div id="pagination_link" style="float: right;"></div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="modal_form" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_input">
                <div class="modal-body">
                    <div class="form-group">
                        <?php $label='title'?>
                        <label>Title</label>
                        <input type="text" class="form-control" name="<?=$label?>" id="<?=$label?>">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group" style="display: none">
                        <?php $label='link_video'?>
                        <label>Title</label>
                        <input type="hidden" class="form-control" name="<?=$label?>" id="<?=$label?>" value="-">
                    </div>
                    <div class="form-group">
                        <?php $label='description'?>
                        <label>Description</label>
                        <textarea class="form-control" name="<?=$label?>" id="<?=$label?>" style="height: 146px;"></textarea>
                    </div>

                    <div class="form-group">
                        <?php $label='image'?>
                        <label>Icon <small>( klik <a target="_blank" href="https://iconify.design/icon-sets/uil/">disini</a> untuk melihat icon )</small></label>
                        <input type="text" class="form-control" name="<?=$label?>" id="<?=$label?>">
                        <a href="" style="float: right">lihat cara menggunakan icon</a>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                <input type="hidden" name="param" id="param" value="add">
                <input type="hidden" name="type" id="type" value="3">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="page" id="page" value="<?=$page?>">
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        load_data(1);
    }).on("click", ".pagination li a", function(event){
        event.preventDefault();
        var page = $(this).data("ci-pagination-page");
        load_data(page);
    });
    function cari(e, val) {
        if (e.keyCode === 13) {
            load_data(1, {search:true, any:val});
        }

    }


    function load_data(page,data={}) {
        $.ajax({
            url:"<?=base_url().'bo/service/load_data/';?>"+page,
            type:"POST",
            data:data,
            dataType:"JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success:function(data)
            {
                $('#result').html(data.result);
                $('#pagination_link').html(data.pagination_link);
            }
        });
    }
    function add() {
        $("#modal_title").text("Add Service");
        $("#param").val("add");
        $("#modal_form").modal("show");
        setTimeout(function () {
            $("#title").focus();
        }, 600);
    }
    function edit(id) {
        $.ajax({
            url: "<?=base_url().'bo/section/edit'?>",
            type: "POST",
            data: {id: id},
            dataType: "JSON",
            beforeSend: function() {
                $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
            },
            complete: function() {
                $('.first-loader').remove();
            },
            success: function (res) {
                if (res.status) {
                    $("#modal_title").text("Edit Slider");
                    $("#param").val("edit");
                    $("#id").val(id);
                    $("#title").val(res.result['title']);
                    $("#description").val(res.result['description']);
                    $("#link").val(res.result['link']);
                    $("#link_video").val(res.result['link_video']);
                    $("#modal_form").modal("show");
                    if($("#page").val()!=='service'){
                        $('#file_uploaded').val((res.result['image']!=''?res.result['image']:''));
                        $('#result_image').attr('src', '<?= base_url() ?>' + (res.result['image']!=''?res.result['image']:'assets/images/no_image.png'));
                    }
                    else{
                        $("#image").val(res.result['image'])
                    }

                    setTimeout(function () {
                        $("#title").focus();
                    }, 600);
                } else {
                    alert("Error getting data!")
                }
            }
        });
    }
    function hapus(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(function(result){
            if(result.isConfirmed){
                $.ajax({
                    url: "<?=base_url().'bo/section/hapus'?>",
                    type: "POST",
                    data: {id: id},
                    beforeSend: function() {
                        $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                    },
                    complete: function() {
                        $('.first-loader').remove();
                    },
                    success: function (res) {
                        if(res){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Data Berhasil Dihapus',
                                showConfirmButton: false,
                                timer: 1500
                            })
//                            Swal.fire(
//                                'Deleted!',
//                                'Your file has been deleted.',
//                                'success'
//                            )
                            load_data(1);
                        }
                        else{
                            alert("Data tidak bisa dihapus!");
                        }
                    },
                    error: function(xhr, status, error) {
                        alert("Data tidak bisa dihapus!");
                    }
                });

            }
            else if (result.dismiss === Swal.DismissReason.cancel) {
                result.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })

    }

    $('#form_input').validate({
        rules: {
            title: {required: true},
            description: {required: true},
            image: {required: true}
        },
        messages: {
            title:{required: "title cannot be empty"},
            description:{required: "description cannot be empty"},
            image:{required: "icon cannot be empty"}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: "<?=base_url().'bo/section/save'?>",
                type: "POST",
                data: $("#form_input").serialize(),
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data(1);
                    $("#modal_form").modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Berhasil Disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });
        }
    });

    $("#modal_form").on("hide.bs.modal", function () {
        document.getElementById("form_input").reset();
        $( "#form_input" ).validate().resetForm();
        $('#result_image').attr('src', '<?= base_url() ?>' + 'assets/images/no_image.png');

    });
</script>