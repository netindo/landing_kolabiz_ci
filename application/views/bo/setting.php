<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 19/01/2021
 * Time: 14:46
 */
?>
<div class="col-12">
    <div class="card height-card box-margin">
        <div class="card-body">
            <p id="isActive"></p>
            <ul class="nav nav-tabs nav-bordered">
                <li class="nav-item">
                    <a href="#account" onclick="handleChangeTab('account')" data-toggle="tab" aria-expanded="false" class="nav-link">
                        Account
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#company" onclick="handleChangeTab('company')" data-toggle="tab" aria-expanded="true" class="nav-link">
                        Company
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#privacy_policy" onclick="handleChangeTab('privacy_policy')" data-toggle="tab" aria-expanded="true" class="nav-link">
                        Privacy Policy
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#faq" onclick="handleChangeTab('faq')" data-toggle="tab" aria-expanded="true" class="nav-link">
                        FAQ
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#other" onclick="handleChangeTab('other')" data-toggle="tab" aria-expanded="false" class="nav-link">
                        Other
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane show" id="account">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <form id="form_account">
                                <div class="form-group">
                                    <?php $label='username';?>
                                    <label class="float-left" for="emailaddress">Username</label>
                                    <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                </div>
                                <div class="form-group">
                                    <?php $label='password';?>
                                    <label class="float-left" for="password">Password</label>
                                    <input class="form-control" type="password" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                </div>

                                <div class="form-group mb-0">
                                    <button class="btn btn-primary btn-block" type="submit"> SAVE </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="company">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <form id="form_company">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php $label='name';?>
                                            <label class="float-left" for="emailaddress">Company Name</label>
                                            <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        </div>
                                        <div class="form-group">
                                            <?php $label='email';?>
                                            <label class="float-left" for="emailaddress">Email</label>
                                            <input class="form-control" type="email" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        </div>
                                        <div class="form-group">
                                            <?php $label='telephone';?>
                                            <label class="float-left" for="emailaddress">Telephone</label>
                                            <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        </div>
                                        <div class="form-group">
                                            <?php $label='address';?>
                                            <label class="float-left" for="emailaddress">Address</label>
                                            <textarea class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>"></textarea>
                                        </div>



                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php $label = 'file_upload'; ?>
                                            <label>Logo</label>
                                            <input type="hidden" id="file_uploaded" name="file_uploaded" />
                                            <input class="form-control" type="file" id="<?=$label?>" name="<?=$label?>" onchange="return ValidateFileUpload()" accept="image/*">
                                            <p class="error" id="alr_<?=$label?>"></p>

                                        </div>
                                        <div class="form-group">
                                            <img src="<?=base_url().'assets/images/no_image.png'?>" id="result_image">

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <button class="btn btn-primary btn-block" type="submit"> SAVE </button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="id_company" name="id_company">

                            </form>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="other">
                    <div class="col-md-12">
                        <form id="form_other">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php $label='version';?>
                                        <label class="float-left" for="emailaddress">Version</label>
                                        <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                        <input class="form-control" type="hidden" name="id_setting" id="id_setting" placeholder="Enter your <?=$label?>">
                                    </div>
                                    <div class="form-group">
                                        <?php $label='url';?>
                                        <label class="float-left" for="emailaddress">Link Website</label>
                                        <input class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>">
                                    </div>
                                    <div class="form-group">
                                        <?php $label='wa_message';?>
                                        <label class="float-left" for="emailaddress">Template Message Whatsapp</label>
                                        <textarea class="form-control" type="text" name="<?=$label?>" id="<?=$label?>" placeholder="Enter your <?=$label?>"></textarea>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button class="btn btn-primary btn-block" type="submit"> SAVE </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="privacy_policy">
                    <div class="col-md-12">
                        <form id="form_privacy_policy">
                            <div class="form-group">
                                <textarea name="desc_privacy" id="desc_privacy"></textarea>
                                <input type="hidden" name="id_desc_privacy" id="id_desc_privacy">
                            </div>
                            <div class="form-group mb-0">
                                <button class="btn btn-primary btn-block" type="submit"> SAVE </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="faq">
                    <div class="col-md-12">
                        <form id="form_faq">
                            <div class="form-group">
                                <textarea name="desc_faq" id="desc_faq"></textarea>
                                <input type="hidden" name="id_desc_faq" id="id_desc_faq">
                            </div>
                            <div class="form-group mb-0">
                                <button class="btn btn-primary btn-block" type="submit"> SAVE </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end card-->
</div> <!-- end col -->

<script>
    $(document).ready(function(){
        CKEDITOR.replace('desc_privacy',{
            filebrowserImageBrowseUrl : '<?=base_url('assets/plugins/kcfinder/browse.php');?>',
            height: '400px',

        });
        CKEDITOR.replace('desc_faq',{
            filebrowserImageBrowseUrl : '<?=base_url('assets/plugins/kcfinder/browse.php');?>',
            height: '400px'
        });
        load_data();
        if(localStorage.isActive===null||localStorage.isActive===undefined){
            $('a[href="#account"]').addClass('active');
            $("#account").addClass('active');
            localStorage.setItem("isActive",'account');
        }
        else{
            $('a[href="#'+localStorage.isActive+'"]').addClass('active');
            $("#"+localStorage.isActive).addClass('active');
        }
    });
    function handleChangeTab(param){
        location.hash = "#"+param;
        localStorage.setItem("isActive",param);
    }

    function load_data(){
        if('<?=$this->session->id==null?>'){
            window.location.href="<?=base_url()?>";
        }
        $.ajax({
            url:"<?=base_url().'bo/setting/load_data/';?>",
            type:"GET",
            dataType:"JSON",
            beforeSend: function() {$('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');},
            complete: function() {$('.first-loader').remove();},
            success:function(data)
            {
                CKEDITOR.instances.desc_privacy.setData(data.setting.privacy_policy);
                CKEDITOR.instances.desc_faq.setData(data.setting.faq);
                loadSetting();
                $("#username").val(data.user.username);
                $("#id_company").val(data.setting.id);
                $("#id_setting").val(data.setting.id);
                $("#id_desc_privacy").val(data.setting.id);
                $("#id_desc_faq").val(data.setting.id);
                $("#name").val(data.setting.name);
                $("#email").val(data.setting.email);
                $("#telephone").val(data.setting.telephone);
                $("#address").val(data.setting.address);
                $("#version").val(data.setting.version);
                $("#url").val(data.setting.url);
                $("#wa_message").val(data.setting.wa_message);
                $('#file_uploaded').val((data.setting.logo!=''?data.setting.logo:''));
                $('#result_image').attr('src', '<?= base_url() ?>' + (data.setting.logo!=''?data.setting.logo:'assets/images/no_image.png'));
            }
        });
    }

    $('#form_account').validate({
        rules: {
            username: {required: true},
        },
        messages: {
            username:{required: "username cannot be empty"},
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_account');
            $.ajax({
                url: "<?=base_url().'bo/setting/update_user'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Has Been Saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(function(){ window.location.href="<?=base_url().'auth'?>"; }, 1500);
                }
            });
        }
    });
    $('#form_company').validate({
        rules: {
            name: {required: true},
            email: {required: true},
            telephone: {required: true},
            address: {required: true}
        },
        messages: {
            name:{required: "company name cannot be empty"},
            email:{required: "email cannot be empty"},
            telephone:{required: "telephone cannot be empty"},
            address:{required: "address cannot be empty"}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_company');
            $.ajax({
                url: "<?=base_url().'bo/setting/update_company'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Has Been Saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
//                    setTimeout(function(){ window.location.href="<?//=base_url().'auth'?>//"; }, 1500);
                }
            });
        }
    });
    $('#form_other').validate({
        rules: {
            version: {required: true},
            url: {required: true},
            wa_message: {required: true}
        },
        messages: {
            version:{required: "version cannot be empty"},
            url:{required: "link website cannot be empty"},
            wa_message:{required: "template message whatsapp cannot be empty"}
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var myForm = document.getElementById('form_other');
            $.ajax({
                url: "<?=base_url().'bo/setting/update_other'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Has Been Saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
//                    setTimeout(function(){ window.location.href="<?//=base_url().'auth'?>//"; }, 1500);
                }
            });
        }
    });
    $('#form_privacy_policy').validate({
        submitHandler: function (form) {
            var myForm = document.getElementById('form_privacy_policy');
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $.ajax({
                url: "<?=base_url().'bo/setting/update_privacy_policy'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    console.log(res);
                    load_data();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Has Been Saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
//                    setTimeout(function(){ window.location.href="<?//=base_url().'auth'?>//"; }, 1500);
                }
            });
        }
    });
    $('#form_faq').validate({
        submitHandler: function (form) {
            var myForm = document.getElementById('form_faq');
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $.ajax({
                url: "<?=base_url().'bo/setting/update_faq'?>",
                type: "POST",
                data: new FormData(myForm),
                mimeType: "multipart/form-data",
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('body').append('<div class="first-loader"><img src="<?=base_url().'/assets/images/spin.svg'?>"></div>');
                },
                complete: function() {
                    $('.first-loader').remove();
                },
                success: function (res) {
                    load_data();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Data Has Been Saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
//                    setTimeout(function(){ window.location.href="<?//=base_url().'auth'?>//"; }, 1500);
                }
            });
        }
    });

    function ValidateFileUpload() {
        var fuData = document.getElementById('file_upload');
        var FileUploadPath = fuData.value;
        var valid = 1;
        $("#alr_file_upload").text("");
        if (FileUploadPath == '') {
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg"|| Extension == "svg") {
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#result_image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(fuData.files[0]);
                }
            }
        }
        return valid;
    }

</script>
