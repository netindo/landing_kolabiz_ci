<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 13:53
 */
?>
<!-- Slider Start -->
<section class="main-slider">
    <ul class="slides">
        <?php foreach($slider as $row): ?>
            <li class="bg-slider d-flex align-items-center" style="background-image:url('<?=base_url().$row["image"]?>')">
                <div class="bg-overlay"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 text-center">
                            <div class="title-heading text-white mt-4">
                                <h1 class="title mb-4"><?=$row["title"]?></h1>
                                <p data-aos="fade-up" data-aos-duration="1000" class="para-desc para-dark mx-auto text-light"><?=$row['description']?></p>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</section><!--end section-->
<!-- Slider End -->
<!-- Service START -->
<section class="section" data-aos="fade-up" data-aos-duration="1000">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="features-absolute">
                    <div class="row">
                        <?php foreach($service as $row): ?>
                            <div class="col-6 col-xs-6 col-md-3" data-aos="fade-up" data-aos-duration="1000" style="margin-bottom: 5px">
                                <div class="card features fea-primary rounded p-4 bg-light text-center position-relative overflow-hidden border-0">
                                    <span class="h1 icon2 text-primary"><i class="uil uil-<?=$row['image']?>"></i></span>
                                    <div class="card-body p-0 content">
                                        <h5><?=$row['title']?></h5>
                                        <p class="para text-muted mb-0"><?=$row['description']?></p>
                                    </div>
                                    <span class="big-icon text-center"><i class="uil uil-<?=$row['image']?>"></i></span>
                                </div>
                            </div><!--end col-->
                        <?php endforeach; ?>
                    </div><!--end row-->
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Service END -->
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
 <!-- DETAIL PAGE -->
<section id="detailContent" class="section bg-light" style = "display:none">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6 col-12">
                
            </div><!--end col-->
        </div><!--end row-->
    </div><!--enc container-->
</section><!--end section-->

<!-- About Start -->
<section class="section bg-light">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6 col-12">
                <img src="<?=base_url().$about['image']?>" class="img-fluid rounded" alt="">
            </div><!--end col-->
            <div class="col-lg-7 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title ml-lg-4">
                    <h4 class="title mb-4"><?=$about['title']?> <span class="text-primary"><?=$setting['name']?></span> ??</h4>
                    <p class="text-muted"><?=$about['description']?></p>
                    <button id="show" class="button detailAbout">Detail</button>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--enc container-->
</section><!--end section-->
<!-- About End -->
<!-- Product Start -->
<style>
    .button{
        background-color: #283593; /* Green */
        border-radius: 5px;
        border: none;
        color: white;
        padding: 10px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .detailAbout {
        background-color: white; 
        color: black; 
        border: 2px solid #283593;
        }

    .detailAbout:hover {
        background-color: #283593;
        color: white;
        }
    #myProgress {
        width: 100%;
        background-color: #ddd;

    }

    #myBar {
        height: 30px;
        background-color: #4CAF50;
        text-align: center;
        line-height: 30px;
        color: white;

    }

</style>
<script>
    $(document).ready(function(){
    $('#show').click(function() {
      $('.detailContent').toggle("slide");
    });
});
</script>
<section class="section" id="produk">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 tit"><span class="tit">PRODUK <span class="text-primary"><?=$setting['name']?></span></span></h4>
                    <p class="text-muted para-desc mx-auto mb-0">BELANJA JADI DUIT</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-md-12">
            <main>
                    <?php foreach($product as $row):?>
                        <article>
<!--                                <ul class="card shop-list border-0 position-relative label list-unstyled mb-0">-->
<!--                                    --><?php //if($row['link']<100):?>
<!--                                        <li><a href="javascript:void(0)" class="badge badge-pill badge-primary">New</a></li>-->
<!--                                    --><?php //else: ?>
<!--                                        <li><a href="javascript:void(0)" class="badge badge-pill badge-warning">Sale</a></li>-->
<!--                                    --><?php //endif; ?>
<!--                                </ul>-->
                                <div class="position-relative d-block overflow-hidden">
                                    <img src="<?=base_url().$row['image']?>" class="img-fluid rounded-top mx-auto" alt="">
                                    <div class="overlay-work bg-dark"></div>
                                    <a href="javascript:void(0)" class="text-white h6 preview">Preview Now <i class="mdi mdi-chevron-right"></i></a>
                                </div>

                                <div class="card-body">
                                    <p><a href="javascript:void(0)" class="title text-dark"><?=$row['title']?></a></p>
                                    <div class="rating">
                                        <ul class="list-unstyled mb-0">
                                            <div id="myProgress">
                                                <div id="myBar" style="width: <?=$row['link']?>%;"></div>
                                            </div>
                                        </ul>
                                    </div>
                                    <div class="fees d-flex justify-content-between">
                                        <ul class="list-unstyled mb-0 numbers">
                                            <li><i class="mdi mdi-progress-clock text-muted"></i> <?=$row['link']?> %</li>
                                            <li><small><i class="mdi mdi-map-marker-radius text-muted"></i> <?=$row['description']?></small></li>
                                        </ul>
                                    </div>
                                </div>
                        </article>

                    <?php endforeach; ?>
            </main>
            </div>

        </div>
    </div>
</section>
<!-- Product End -->
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->
<!-- Kerjasama Start -->
<section class="section bg-light" id="kerjasama">
    <div class="container">
        <div class="row align-items-center">
        <h4 class="title mb-4 tit"><span class="tit">Happy Shopping <span class="text-primary"><?=$setting['name']?></span></span></h4>
            <div class="col-lg-6 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="section-title">
                <h5 class="text-muted para-desc mx-auto mb-4">Beli cuma 150.000, dapet produk keren</h5>
                    <ul class="list-unstyled text-muted">
                        <?php foreach ($kerjasama as $row):?>
                            <li class="mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span><?=$row['description']?></li>
                        <?php endforeach;?>
                    </ul>
<!--                    <a href="javascript:void(0)" class="btn btn-primary mt-3">Get Started <i class="mdi mdi-chevron-right"></i></a>-->
                </div>
            </div><!--end col-->
            <div class="col-lg-6 col-md-6 order-1 order-md-2">
                <img src="<?=base_url().'assets/'?>images/happy.svg" class="img-fluid" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
<!-- Kerjasama End -->
<style>
    h4.tit {
        width: 100%;
        text-align: center;
        border-bottom: 1px solid #E5E5E5;
        line-height: 0.1em;
        margin: 10px 0 20px;
    }

    h4.tit span.tit {
        background:#fff;
        padding:0 10px;
    }
</style>
<!-- Rules Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 tit"><span class="tit">MENGAPA BISNIS INI MENGUNTUNGKAN</span></h4>
                    <p class="text-muted para-desc mx-auto mb-0">
                        Jangan ragu menjadi member KOLABIZ.
                        Beragam keuntungan dapat diperoleh.
                    </p>

                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <?php foreach($workFlow as $key=>$row):?>
                <div class="col-12 col-xs-12 col-sm-6 col-md-4 mt-4 pt-2">
                    <div class="card work-process border-0 text-center rounded shadow pricing-rates">
                        <div class="card-body">
                            <img src="<?=base_url().$row['image']?>" class="avatar avatar-large mb-3" alt="">
                            <h4 class="title"><?=$row['title']?></h4>
                            <p class="text-muted para">
                                <?=$row['description']?>
                            </p>
                            <ul class="list-unstyled d-flex justify-content-between mb-0 mt-2">
                                <li class="step h1 mb-0 font-weight-bold">Profit 0<?=$key+1?>.</li>
                                <li class="step-icon"><i class="mdi mdi-chevron-double-right mdi-36px"></i></li>
                            </ul>
                        </div>
                    </div>
                </div><!--end col-->
            <?php endforeach;?>

        </div><!--end row-->
    </div><!--end container-->
</section>
<!-- Rules End -->
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Shape End-->
<!-- BIMTEK Start -->
<section class="section bg-light" id="konsultan">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title">
                    <h4 class="title mb-4 tit"><span style="background-color: #f8f9fc !important" class="tit">POIN REDEEM</span></h4>
                    <p class="text-muted para-desc mb-0 mx-auto">
                        Tukarkan Poin dengan Hadiah
                    </p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->


    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-7 order-1 order-md-2">
                <img src="<?=base_url().'assets/'?>images/poin_fix.svg" class="img-fluid"    alt="">
            </div><!--end col-->
            <div class="col-lg-7 col-md-5 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="faq-content mt-4 pt-2">
                    <div class="accordion" id="accordionExampleone">
                        <?php foreach($bimtek as $key=>$row):?>
                            <div class="card border-0 rounded shadow mb-2">
                                <a data-toggle="collapse" href="#collapse<?=$key?>" class="faq position-relative" aria-expanded="<?=$key==0?'true':'false'?>" aria-controls="collapse<?=$key?>">
                                    <div class="card-header border-0 bg-light p-3 pr-5" id="headingfifone">
                                        <h6 class="title mb-0"><?=$row['title']?></h6>
                                    </div>
                                </a>
                                <div id="collapse<?=$key?>" class="collapse <?=$key==0?'show':''?>" aria-labelledby="headingfifone" data-parent="#accordionExampleone">
                                    <div class="card-body px-2 py-4">
                                        <p class="text-muted mb-0 faq-ans"><?=$row['description']?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>
<!-- BIMTEK End -->
<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-light">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!-- Shape End -->
<!-- Gallery Start -->
<section class="section" id="galeri">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 tit"><span class="tit">Aktivitas <span class="text-primary"><?=$setting['name']?></span></span></h4>

                    <p class="text-muted para-desc mx-auto mb-0">
                        Berikut beberapa potret aktivas dari KOLABIZ
                    </p>
                </div>
            </div><!--end col-->

        </div><!--end row-->
        <div class="row justify-content-center">
            <div class="col-lg-12 mt-4">
                <div id="myExample1">
                    <?php foreach ($gallery as $row):?>
                        <a href="<?=base_url().$row['image']?>" class="image-link" title="<?=$row['title']?>">
                            <img alt="<?=$row['title']?>" src="<?=base_url().$row['image']?>" />
                        </a>
                    <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Gallery End -->

<!-- Testimoni Start-->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 tit"><span class="tit">Apa Kata Mereka Tentang <span class="text-primary"><?=$setting['name']?></span></span></h4>
                    <p class="text-muted para-desc mx-auto mb-0">Kumpulan testimoni</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row justify-content-center">
            <div class="col-lg-12 mt-4">
                <div id="customer-testis" class="owl-carousel owl-theme">
                    <?php foreach($testimoni as $row):?>
                        <div class="col-12 col-xs-12 media customer-testi m-2">
                            <img src="<?=base_url().$row['image']?>" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2"><?=$row['description']?></p>
                                <h6 class="text-primary"><?=$row['title']?></h6>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section>


<section class="sectio bg-light">
    <div class="container">
        <div class="row align-items-end mb-4 pb-4">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4 tit"><span class="tit">Tentang <span class="text-primary"><?=$setting['name']?></span></span></h4>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-md-4 mt-4 pt-2">
                <ul class="nav nav-pills nav-justified flex-column bg-white rounded shadow p-3 mb-0 sticky-bar" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link rounded active" id="webdeveloping" data-toggle="pill" href="#developing" role="tab" aria-controls="developing" aria-selected="false">
                            <div class="text-center py-1">
                                <h6 class="mb-0">Syarat & Ketentuan</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->

                    <li class="nav-item mt-2">
                        <a class="nav-link rounded" id="database" data-toggle="pill" href="#data-analise" role="tab" aria-controls="data-analise" aria-selected="false">
                            <div class="text-center py-1">
                                <h6 class="mb-0">FAQ</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                    <li class="nav-item mt-2">
                        <a class="nav-link rounded" id="contact" data-toggle="pill" href="#data-contact" role="tab" aria-controls="data-contact" aria-selected="false">
                            <div class="text-center py-1">
                                <h6 class="mb-0">HUBUNGI KAMI</h6>
                            </div>
                        </a><!--end nav link-->
                    </li><!--end nav item-->
                </ul><!--end nav pills-->
            </div><!--end col-->

            <div class="col-md-8 col-12 mt-4 pt-2">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade bg-white show active p-4 rounded shadow" id="developing" role="tabpanel" aria-labelledby="webdeveloping">
                        <div class="mt-4">
                            <p class="text-muted"><?=$setting['privacy_policy']?></p>
                        </div>
                    </div><!--end teb pane-->

                    <div class="tab-pane fade bg-white p-4 rounded shadow" id="data-analise" role="tabpanel" aria-labelledby="database">
                        <div class="mt-4">
                            <p class="text-muted"><?=$setting['faq']?></p>
                        </div>
                    </div><!--end teb pane-->

                    <div class="tab-pane fade bg-white p-4 rounded shadow" id="data-contact" role="tabpanel" aria-labelledby="contact">
                        <div class="mt-4">
                            <p class="text-muted">
                                <img src="<?=base_url().$setting['logo']?>" style="width: 100%;margin-bottom: 10px" alt=""><br>
                                <?=$setting['name']?><br>
                                <i class="mdi mdi-map-marker-radius text-muted"></i> <?=$setting['address']?><br>
                                <i class="mdi mdi-file-phone-outline text-muted"></i> <a href="https://api.whatsapp.com/send?phone=<?=$setting["telephone"]?>&amp;text=<?=$setting["wa_message"]?>" target="_blank"><?=$setting['telephone']?></a><br>
                                <i class="mdi mdi-email-edit text-muted"></i> <a href="mailto:<?=$setting['email']?>"> <?=$setting['email']?></a><br>
                                <div id="googleMap" style="width:100%;height:400px;"></div>
                            </p>
                        </div>
                    </div><!--end teb pane-->


                </div><!--end tab content-->
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->

<!--Shape End-->
<!-- Contact Start -->
<section class="section bg-dark" id="invest" style="background-color: #202942!important;">
    <!--    <div class="bg-overlay"></div>-->
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">

                <div class="row" id="counter">
                    <div class="col-12 text-center">
                        <div class="section-title">
                            <h4 class="title text-white">AYO JADI MEMBER SEKARANG</h4>
                        </div>

                        <div class="contact-detail">
                            <a href="https://api.whatsapp.com/send?phone=<?=$setting["telephone"]?>&amp;text=<?=$setting["wa_message"]?>" target="_blank" class="btn btn-primary mt-3">Hubungi WA   <i class="uil uil-comment-chart-line"></i></a>
                            <div class="content mt-3 overflow-hidden d-block">
                                <h5 class="text-muted"><a href="https://api.whatsapp.com/send?phone=<?=$setting["telephone"]?>&amp;text=<?=$setting["wa_message"]?>" class="text-primary h5"><?=$setting["telephone"]?></a></h5>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end col-->


        </div><!--end row-->
    </div><!--end container-->
</section>
<!-- Contact End -->

<style>
    article {
        padding: 0px;
        border: 1px solid #E5E5E5;
        border-radius: 8px;
        /*-webkit-column-count: 4;*/
        /*-moz-column-count:4;*/
        /*column-count: 4;*/
        /*-webkit-column-gap: 1em;*/
        /*-moz-column-gap: 1em;*/
        /*column-gap: 1em;*/
        /*margin: 1.5em;*/
        /*padding: 0;*/
        /*-moz-column-gap: 1.5em;*/
        /*-webkit-column-gap: 1.5em;*/
        /*column-gap: 1.5em;*/
        /*background: #fff;*/
    }
    a:link,
    a:visited {
        color: black;
    }
    h1 {
        margin-top: 0;
        margin-left: 0.75rem;
    }
    main {
        columns: 250px;
        column-gap: 20px;
    }
    article {
        break-inside: avoid-column;
        margin-bottom: 1rem;
    }
    /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) {
        main{
            -moz-column-count: 2;
            -webkit-column-count: 2;
            column-count: 2;
        }
        h1.title{
            font-size: 18px!important;
        }
        .tit{
            font-size: 12px!important;
        }

    }

    /* Small devices (portrait tablets and large phones, 600px and up) */
    @media only screen and (min-width: 600px) {...}

    /* Medium devices (landscape tablets, 768px and up) */
    @media only screen and (min-width: 768px) {...}

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {...}

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {...}
</style>


