<?php
/**
 * Created by PhpStorm.
 * User: annashrul_yusuf
 * Date: 15/01/2021
 * Time: 11:25
 */

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?=$setting['name']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="Shreethemes" />
    <meta name="email" content="shreethemes@gmail.com" />
    <meta name="website" content="http://www.shreethemes.in/" />
    <meta name="Version" content="v2.6" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Edu+SA+Beginner&family=Signika+Negative&display=swap" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href="<?=base_url().$setting['logo']?>">
    <!-- Bootstrap -->
    <link href="<?=base_url().'assets/'?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="<?=base_url().'assets/'?>css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=base_url().'assets/'?>css/line.css">
    <!-- Magnific -->
    <link href="<?=base_url().'assets/'?>css/magnific-popup.css" rel="stylesheet" type="text/css" />
    <!-- Slider -->
    <link rel="stylesheet" href="<?=base_url().'assets/'?>css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="<?=base_url().'assets/'?>css/owl.theme.default.min.css"/>
    <!-- FLEXSLIDER -->
    <link href="<?=base_url().'assets/'?>css/flexslider.css" rel="stylesheet" type="text/css" />
    <!-- Main Css -->
    <link href="<?=base_url().'assets/'?>css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="<?=base_url().'assets/'?>css/colors/default.css" rel="stylesheet" id="color-opt">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.css" integrity="sha512-4a1cMhe/aUH16AEYAveWIJFFyebDjy5LQXr/J/08dc0btKQynlrwcmLrij0Hje8EWF6ToHCEAllhvGYaZqm+OA==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" integrity="sha512-nIm/JGUwrzblLex/meoxJSPdAKQOe2bLhnrZ81g5Jbh519z8GFJIWu87WAhBH+RAyGbM4+U3S2h+kL5JoV6/wA==" crossorigin="anonymous" />


    <style>
        *{
            font-family: 'Signika Negative', sans-serif;
        }
        html {
            scroll-behavior: smooth;
        }

    </style>
    <!-- ==== Google Map API ==== -->
</head>

<body>
<!-- Navbar STart -->
<header id="topnav" class="defaultscroll sticky">
    <div class="container">
        <div>
            <a class="logo" href="javascript:void(0)">
                <img src="<?=base_url().$setting['logo']?>" class="l-dark" height="40" alt="">
                <img src="<?=base_url().$setting['logo']?>" class="l-light" height="40" alt="">
            </a>
        </div>
        <div class="buy-button">
            <a href="https://dash.kolabiz.id/">
                <div class="btn btn-primary login-btn-primary">Login Member</div>
                <div class="btn btn-light login-btn-light">Login Member</div>
            </a>
        </div><!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
	            <div class="menu-item">
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
            </div>
        </div>

        <div id="navigation">
            <ul class="navigation-menu nav-light">
                <li><a href="#produk">Produk Kami</a></li>
                <li><a href="#konsultan">Poin</a></li>
                <li><a href="#kerjasama">Happy Shopping</a></li>
                <li><a href="#galeri">Galeri</a></li>
            </ul>
            <div class="buy-menu-btn d-none">
                <a href="https://dash.kolabiz.id/" class="btn btn-primary">Login  Member</a>
            </div>
        </div>
    </div>
</header>
<!-- Navbar End -->

<?php $this->load->view($content);?>

<!-- Footer Start -->
<!--<section class="bg-light">-->
<!--    <div id="googleMap" style="width:100%;height:400px;"></div>-->
<!---->
<!--</section>-->
<!--end footer-->

<!-- Footer Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-footer">
        <svg viewBox="0 0 2880 250" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M720 125L2160 0H2880V250H0V125H720Z" fill="currentColor"></path>
        </svg>
    </div>
</div>

<footer class="footer footer-bar">
    <div class="container text-center">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <div class="text-sm-left">
                    <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> <?=$setting['name']?></p>
                </div>
            </div><!--end col-->

            <div class="col-sm-4 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <ul class="list-unstyled text-sm-right mb-0">
                    <?php foreach ($socmed as $item):?>
                        <li class="list-inline-item"><a href="<?=$item['link']?>"><img style="max-height: 20px!important;" src="<?=base_url().$item['image']?>" title="<?=$item['title']?>" alt="<?=$item['title']?>"></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Back to top -->
<a href="#" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->
<!-- Back to top -->
<a href="#" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
<!-- Back to top -->

<!-- javascript -->
<script src="<?=base_url().'assets/'?>js/jquery-3.5.1.min.js"></script>
<script src="<?=base_url().'assets/'?>js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url().'assets/'?>js/jquery.easing.min.js"></script>
<script src="<?=base_url().'assets/'?>js/scrollspy.min.js"></script>
<script src="<?=base_url().'assets/'?>js/jquery.magnific-popup.min.js"></script>
<script src="<?=base_url().'assets/'?>js/magnific.init.js"></script>
<script src="<?=base_url().'assets/'?>js/owl.carousel.min.js"></script>
<script src="<?=base_url().'assets/'?>js/owl.init.js"></script>
<script src="<?=base_url().'assets/'?>js/jquery.flexslider-min.js"></script>
<script src="<?=base_url().'assets/'?>js/flexslider.init.js"></script>
<script src="<?=base_url().'assets/'?>js/counter.init.js"></script>
<script src="<?=base_url().'assets/'?>js/feather.min.js"></script>
<script src="<?=base_url().'assets/'?>js/bundle.js"></script>
<script src="<?=base_url().'assets/'?>js/switcher.js"></script>
<script src="<?=base_url().'assets/'?>js/app.js"></script>
<script src="<?=base_url().'assets/'?>js/aos.js"></script>
<link rel='stylesheet' href="<?=base_url().'assets/plugins/justified-gallery/css/justifiedGallery.css'?>" type='text/css' media='all' />
<script type='text/javascript' src='<?=base_url().'assets/plugins/justified-gallery/js/jquery.justifiedGallery.js'?>'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js" integrity="sha512-+m6t3R87+6LdtYiCzRhC5+E0l4VQ9qIT1H9+t1wmHkMJvvUQNI5MKKb7b08WL4Kgp9K0IBgHDSLCRJk05cFUYg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.js" integrity="sha512-/jeu5pizOsnKAR+vn28EbhN5jDSPTTfRzlHZh8qSqB77ckQd3cOyzCG9zo20+O7ZOywiguSe+0ud+8HQMgHH9A==" crossorigin="anonymous"></script>

<style>
    .mfp-s-loading .mfp-arrow {
        display: none;
    }
    a.hasyTc{
        display: none!important;
    }
   .Window__WindowComponent-sc-17wvysh-1{
       border: 1px solid black!important;
   }
</style>

<script>
    if(window.matchMedia("(max-width: 767px)").matches){
        // The viewport is less than 768 pixels wide
        $('#customer-testis').owlCarousel({
            loop:true,
            nav: false,
            dots: true,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            responsive : {
                480 : { items : 1  }, // from zero to 480 screen width 4 items
                768 : { items : 1  }, // from 480 screen widthto 768 6 items
                1024 : { items : 1   // from 768 screen width to 1024 8 items
                }
            },
        });
    } else{
        // The viewport is at least 768 pixels wide
        $('#customer-testis').owlCarousel({
            loop:true,
            nav: false,
            dots: true,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            responsive : {
                480 : { items : 1  }, // from zero to 480 screen width 4 items
                768 : { items : 1  }, // from 480 screen widthto 768 6 items
                1024 : { items : 2   // from 768 screen width to 1024 8 items
                }
            },
        });
    }

</script>

<!-- GetButton.io widget -->
<script type="text/javascript">



        (function () {
        var options = {
            whatsapp: "<?=$setting['telephone']?>", // WhatsApp number
            email: "<?=$setting['email']?>", // Email
            call_to_action: "Hubungi Kami", // Call to action
            button_color: "#2f55d4", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "whatsapp,email", // Order of buttons
            pre_filled_message: "<?=$setting['wa_message']?>" // WhatsApp pre-filled message
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->
<!-- /GetButton.io widget -->
<script>
    AOS.init({
        easing: 'ease-in-out-sine',
        duration: 1000
    });
</script>

<script type="text/javascript">
    $("#myExample1").justifiedGallery({
        rowHeight:150
    });
    $('#myExample1').magnificPopup({
        type: 'image',
        preloader: true,
        delegate: 'a',
        gallery:{enabled:true},
        callbacks: {
            buildControls: function() {
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(-6.899541,107.533867),
            zoom:5,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqD1Z03FoLnIGJTbpAgRvjcchrR-NiICk&callback=myMap"></script>

</body>
</html>
